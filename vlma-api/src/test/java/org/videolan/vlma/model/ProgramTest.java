/*
 * Copyright (C) 2008 the VideoLAN team
 *
 * This file is part of VLMa.
 *
 * VLMa is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * VLMa is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with VLMa. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package org.videolan.vlma.model;

import static org.junit.Assert.*;

import java.util.Date;

import org.junit.Test;

public class ProgramTest {

    @Test
    public void testIsTimeToPlayOnInstantiation() {
        Program program = new Program();
        assertTrue(program.isTimeToPlay());
    }

    @Test
    public void testNotStartedYet() {
        Program program = new Program();
        program.setLiveStart(new Date(System.currentTimeMillis() + 10000));
        assertFalse(program.isTimeToPlay());
    }

    @Test
    public void testAlreadyStarted() {
        Program program = new Program();
        program.setLiveStart(new Date());
        assertTrue(program.isTimeToPlay());
    }

    @Test
    public void testLiveExpired() {
        Program program = new Program();
        program.setLiveStart(new Date(System.currentTimeMillis() - 1000));
        program.setLiveLength(800);
        assertFalse(program.isTimeToPlay());
    }

    @Test
    public void testLiveNotExpiredYet() {
        Program program = new Program();
        program.setLiveStart(new Date(System.currentTimeMillis() - 1000));
        program.setLiveLength(2000);
        assertTrue(program.isTimeToPlay());
    }

    @Test
    public void testInactiveSlot() {
        Program program = new Program();
        program.setLiveStart(new Date(System.currentTimeMillis() - 1000));
        program.setLiveLength(600);
        program.setLiveInterval(2000);
        assertFalse(program.isTimeToPlay());
    }

    @Test
    public void testActiveSlot() {
        Program program = new Program();
        program.setLiveStart(new Date(System.currentTimeMillis() - 1000));
        program.setLiveLength(600);
        program.setLiveInterval(900);
        assertTrue(program.isTimeToPlay());
    }

}
