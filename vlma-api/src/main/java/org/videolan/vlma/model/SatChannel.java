/*
 * Copyright (C) 2006-2008 the VideoLAN team
 *
 * This file is part of VLMa.
 *
 * VLMa is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * VLMa is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with VLMa. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package org.videolan.vlma.model;


/**
 * A satellite channel.
 *
 * @author Sylvain Cadilhac <sylv at videolan.org>
 */
public class SatChannel extends DVBMedia {

    private static final long serialVersionUID = 3L;

    private char polarisation;

    private String fullname;

    private String country;

    private String category;

    private String encryption;

    private int errorCorrection;

    private int symbolRate;

    private String coverage;

    public static final String DEFAULT_SAT_SAP_GROUP = "Satellite";

    public static final String DEFAULT_RADIO_SAP_GROUP = "Radio";

    /**
     * Constructs a new SatChannel.
     */
    public SatChannel() {
        super();
        this.category = "R-DIG";
        this.coverage = "";
    }

    /**
     * Gets the channel category (TV, radio, encrypted, etc.).
     *
     * @return the channel category
     */
    public String getCategory() {
        return category;
    }

    /**
     * Sets the channel category.
     *
     * @param category
     *            the channel category to set
     */
    public void setCategory(String category) {
        this.category = category;
    }

    /**
     * Gets the channel country.
     *
     * @return the country
     */
    public String getCountry() {
        return country;
    }

    /**
     * Sets the channel country.
     *
     * @param country
     *            the country to set
     */
    public void setCountry(String country) {
        this.country = country;
    }

    /**
     * Gets the channel encryption.
     *
     * @return the channel encryption
     */
    public String getEncryption() {
        return encryption;
    }

    /**
     * Sets the encryption of the channel.
     *
     * @param encryption
     *            the encryption to set
     */
    public void setEncryption(String encryption) {
        this.encryption = encryption;
    }

    /**
     * Gets the error correction.
     *
     * @return the error correction
     */
    public int getErrorCorrection() {
        return errorCorrection;
    }

    /**
     * Sets the error correction.
     *
     * @param errorCorrection
     *            the error correction to set
     */
    public void setErrorCorrection(int errorCorrection) {
        this.errorCorrection = errorCorrection;
    }

    /**
     * Gets the channel polarisation.
     *
     * @return the channel polarisation
     */
    public char getPolarisation() {
        return polarisation;
    }

    /**
     * Sets the channel polarisation.
     *
     * @param polarisation
     *            the channel polarisation
     */
    public void setPolarisation(char polarisation) {
        this.polarisation = polarisation;
    }

    /**
     * Gets the symbol rate.
     *
     * @return the rate
     */
    public int getSymbolRate() {
        return symbolRate;
    }

    /**
     * Sets the symbol rate.
     *
     * @param symbolRate
     *            the symbol rate to set
     */
    public void setSymbolRate(int symbolRate) {
        this.symbolRate = symbolRate;
    }

    /**
     * Gets the channel fullname.
     *
     * @return the channel fullname
     */
    public String getFullname() {
        return fullname;
    }

    /**
     * Sets the channel fullname.
     *
     * @param fullname
     *            the name to set
     */
    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    /**
     * Gets the channel coverage zone.
     *
     * @return the coverage zone
     */
    public String getCoverage() {
        return coverage;
    }

    /**
     * Sets the coverage zone.
     *
     * @param coverage
     *            the coverage zone to set
     */
    public void setCoverage(String coverage) {
        this.coverage = coverage;
    }

    /**
     * Compares this object against the specified object. The result is true
     * if and only if the argument has same coverage zone, same frequency and
     * same name.
     *
     * @return true if objects are the same; false otherise
     */
    @Override
    public boolean equals(Object o) {
        if (o == null) return false;
        if (o.getClass().equals(SatChannel.class)) {
            SatChannel c = (SatChannel) o;
            return c.getCoverage().equals(coverage)
                    && c.getFrequency() == frequency
                    && c.sid == sid;
        }
        return false;
    }

    /**
     * Gets the channel hash code.
     *
     * @return the hashcode
     */
    @Override
    public int hashCode() {
        return frequency + coverage.hashCode() + name.hashCode();
    }

    public boolean getIsRadio() {
        return category.contains("R-DIG");
    }

    public boolean getIsCrypted() {
        return category.contains("CRYPT");
    }

}
