/*
 * Copyright (C) 2009 the VideoLAN team
 *
 * This file is part of VLMa.
 *
 * VLMa is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * VLMa is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with VLMa. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package org.videolan.vlma.model;

import org.videolan.vlma.model.StreamingStrategy.Type;

/**
 * Group of programs associated with a FilesChannel.
 * Two FilesChannels programs can be in the same group only if:
 *  * they are associated with the same media (same input),
 *  * they are no VOD (because it would need two asynchronous reads of the
 *    input).
 *
 * @author Adrien Grand
 */
public class FilesProgramGroup extends ProgramGroup {

    private static final long serialVersionUID = 1L;

    private Type type;
    private FilesChannel media;

    public FilesChannel getMedia() {
        return media;
    }

    @Override
    protected boolean isAddable(Program program) {
        // Only one VOD per group
        return program.getMedia() instanceof FilesChannel
                && (isEmpty() || (media.equals(program.getMedia())
                        && type == Type.BROADCAST
                        && program.getStreamingStrategy().getType() == Type.BROADCAST));
    }

    @Override
    public boolean add(Program program) {
        boolean empty = isEmpty();
        boolean result = super.add(program);
        if (empty) {
            media = (FilesChannel)program.getMedia();
            type = program.getStreamingStrategy().getType();
        }
        return result;
    }
}
