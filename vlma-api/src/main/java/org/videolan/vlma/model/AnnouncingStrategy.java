/*
 * Copyright (C) 2008 the VideoLAN team
 *
 * This file is part of VLMa.
 *
 * VLMa is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * VLMa is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with VLMa. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package org.videolan.vlma.model;

import java.io.Serializable;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

/**
 * The announcing strategy. Describes how a media should be announced.
 *
 * @author Adrien Grand
 */
public class AnnouncingStrategy implements Serializable {

    private static final long serialVersionUID = 1L;

    public static enum Announcement {
        SAP("SAP"),
        PODCAST("Podcast"),
        M3U("Playlist (M3U)");
        private Announcement(String name) { this.name = name; }
        private String name;
        public String getName() { return name; }
    }

    private Set<Announcement> announcements;

    public AnnouncingStrategy() {
        announcements = new HashSet<Announcement>();
    }

    /**
     * Gets the announcement list.
     *
     * @return the announcements
     */
    public Set<Announcement> getAnnouncements() {
        return announcements;
    }

    /**
     * Adds an announcement.
     *
     * @param announcement the announcement to add
     */
    public void addAnnouncement(Announcement announcement) {
        this.announcements.add(announcement);
    }

    /**
     * Verifies whether or not an announcement is enabled.
     *
     * @return true if and only if the announcement is enabled
     */
    public boolean isEnabled(Announcement announcement) {
        return announcements.contains(announcement);
    }

    /**
     * Sets the announcements.
     *
     * @param announcements the announcements to set
     */
    public void setAnnouncements(Collection<Announcement> announcements) {
        this.announcements = new HashSet<Announcement>();
        this.announcements.addAll(announcements);
    }

    @Override
    public boolean equals(Object o) {
        if (o == null) return false;
        if (o instanceof AnnouncingStrategy) {
            return announcements.equals(((AnnouncingStrategy)o).getAnnouncements());
        }
        return false;
    }

    @Override
    public int hashCode() {
        return announcements.hashCode();
    }

}
