/*
 * Copyright (C) 2006-2008 the VideoLAN team
 *
 * This file is part of VLMa.
 *
 * VLMa is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * VLMa is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with VLMa. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package org.videolan.vlma.model;

import java.io.Serializable;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;

/**
 * A streaming server.
 *
 * @author Sylvain Cadilhac <sylv at videolan.org>
 */
public class Server implements Serializable {

    private static final long serialVersionUID = 1L;

    private Integer id;

    private List<Adapter> adapters;

    private String address;
    InetAddress ip;

    private boolean isUp;

    private String name;

    /**
     * Default constructor.
     * @throws UnknownHostException
     *
     */
    public Server() throws UnknownHostException {
        adapters = new ArrayList<Adapter>();
        setUp(true);
        this.ip = InetAddress.getLocalHost();
        this.address = ip.getHostName();
    }

    /**
     * Class constructor specifying name and IP address.
     *
     * @param name the server name
     * @param ip   the server IP address
     * @throws UnknownHostException
     */
    public Server(String name, String address) throws UnknownHostException {
        this();
        this.name = name;
        this.address = address;
        this.ip = InetAddress.getByName(address);
    }

    @Override
    public boolean equals(Object o) {
        if (o == null || !(o instanceof Server))
            return false;
        Server server = (Server) o;
        return server.ip.equals(ip);
    }

    /**
     * Gets the server adapters.
     *
     * @return a map of the server adapters
     */
    public List<Adapter> getAdapters() {
        return adapters;
    }

    /**
     * Add an adapter to this server.
     *
     * @param adapter the adapter to add.
     */
    public void addAdapter(Adapter adapter) {
        adapters.add(adapter);
    }

    /**
     * Remove the provided adapter from this server.
     *
     * @param adapter the adapter to remove
     */
    public void removeAdapter(Adapter adapter) {
        adapters.remove(adapter);
    }

    /**
     * Gives the server ID.
     *
     * @return the server ID
     */
    public int getId() {
        return id;
    }

    /**
     * Sets the server id.
     *
     * @param id the id to set
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * Get the server address (DNS or IP)
     *
     * @return the server address
     */
    public String getAddress() {
        return address;
    }

    /**
     * Gets the server IP address.
     *
     * @return the server ip address
     * @throws UnknownHostException
     */
    public InetAddress getIp() throws UnknownHostException {
        ip = InetAddress.getByName(address);
        return ip;
    }

    /**
     * Gives the server name.
     *
     * @return the server name
     */
    public String getName() {
        return name;
    }

    /**
     * @see Server#isUp
     */
    public boolean getUp() {
        return isUp();
    }

    @Override
    public int hashCode() {
        return ip.hashCode();
    }

    /**
     * Returns the server ability to receive orders.
     *
     * @return true if the server is up, false otherwise
     */
    public boolean isUp() {
        return isUp;
    }

    /**
     * Sets the server address.
     *
     * @param address the server DNS or IP address
     * @throws UnknownHostException
     */
    public void setAddress(String address) throws UnknownHostException {
        ip = InetAddress.getByName(address);
        this.address = address;
    }

    /**
     * Sets the server name.
     *
     * @param name the server name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Sets the server availability.
     *
     * @param isUp true if the server is available, false otherwise
     */
    public void setUp(boolean isUp) {
        this.isUp = isUp;
    }

    @Override
    public String toString() {
        return name;
    }

}
