/*
 * Copyright (C) 2009 the VideoLAN team
 *
 * This file is part of VLMa.
 *
 * VLMa is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * VLMa is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with VLMa. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package org.videolan.vlma.model;


/**
 * A group of programs associated with a satellite channel.
 * Two programs associated with a SatChannel can be in the same group if:
 *  * both medias have the same frequency,
 *  * both medias are available on the same satellite.
 *
 * @author Adrien Grand
 */
public class SatProgramGroup extends DVBProgramGroup {

    private static final long serialVersionUID = 1L;

    private char polarisation;
    private String coverage;
    private int symbolRate;
    private int errorCorrection;

    public SatProgramGroup() {
        coverage = null;
        frequency = -1;
        polarisation = 0;
        symbolRate = 27500;
        errorCorrection = 9;
    }

    public String getCoverage() {
        return coverage;
    }

    public char getPolarisation() {
        return polarisation;
    }

    public int getErrorCorrection() {
        return errorCorrection;
    }

    public int getSymbolRate() {
        return symbolRate;
    }

    @Override
    protected boolean isAddable(Program program) {
        if (program.getMedia() instanceof SatChannel) {
            SatChannel channel = (SatChannel) program.getMedia();
            return isEmpty() || (frequency == channel.getFrequency()
                    && polarisation == channel.getPolarisation()
                    && coverage.equals(channel.getCoverage()));
        }
        return false;
    }

    @Override
    public boolean add(Program program) {
        boolean empty = isEmpty();
        boolean result = super.add(program);
        if (empty) {
            SatChannel channel = (SatChannel) program.getMedia();
            coverage = channel.getCoverage();
            frequency = channel.getFrequency();
            polarisation = channel.getPolarisation();
            symbolRate = channel.getSymbolRate();
            errorCorrection = channel.getErrorCorrection();
        }
        return result;
    }
}
