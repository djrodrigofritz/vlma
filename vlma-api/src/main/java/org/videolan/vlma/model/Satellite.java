/*
 * Copyright (C) 2006-2008 the VideoLAN team
 *
 * This file is part of VLMa.
 *
 * VLMa is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * VLMa is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with VLMa. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package org.videolan.vlma.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * A satellite.
 *
 * @author Sylvain Cadilhac <sylv at videolan.org>
 */
public class Satellite implements Serializable {

    private static final long serialVersionUID = -341241294380630516L;

    private Integer id;

    private String name;

    private List<String> coverages;

    /**
     * Constructs a satellite with the specified name.
     *
     * @param name
     *            the satellite name
     */
    public Satellite(String name) {
        this.name = name;
        coverages = new ArrayList<String>();
    }

    /**
     * Gets the coverage zones of the satellite.
     *
     * @return the coverage zones
     */
    public List<String> getCoverages() {
        return coverages;
    }

    /**
     * Gets the satellite name.
     *
     * @return the satellite name
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the satellite name.
     *
     * @param name
     *            the satellite name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Compares two satellites. Returns true if and only if satellites have the same name.
     *
     * @param o
     *            the object to be compared for equality
     * @return true if satellites have the same name; false otherwise
     */
    @Override
    public boolean equals(Object o) {
        if (o == null) return false;
        if (o.getClass().equals(Satellite.class)) {
            return ((Satellite) o).name.equals(this.name);
        } else {
            return false;
        }
    }

    /**
     * Gets the hash code.
     *
     * @return the hash code
     */
    @Override
    public int hashCode() {
        return name.hashCode();
    }

    /**
     * Gets the satellite ID.
     *
     * @return the satellite ID
     */
    public Integer getId() {
        return id;
    }

    /**
     * Sets the satellite ID.
     */
    public void setId(int id) {
        this.id = id;
    }
}
