/*
 * Copyright (C) 2006-2008 the VideoLAN team
 *
 * This file is part of VLMa.
 *
 * VLMa is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * VLMa is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with VLMa. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package org.videolan.vlma.model;


/**
 * This class represents a channel where outer streams are diffused.
 *
 * @author Adrien Maglo <magsoft at videolan.org>
 */
public class StreamChannel extends RemoteInputMedia {

    private static final long serialVersionUID = 3L;

    /**
     *  The URL of the stream
     */
    private String streamURL;

    /**
     * The constructor of the class
     */
    public StreamChannel() {
        super();
        streamURL = "udp://@";
    }

    public String getStreamURL() {
        return this.streamURL;
    }

    public void setStreamURL(String URL) {
        this.streamURL = URL;
    }

    /**
     * Compares two StreamChannels.
     *
     * @return true if objects are the same, false otherwise
     */
    @Override
    public boolean equals(Object o) {
        if (o == null) return false;
        if (o instanceof StreamChannel) {
            StreamChannel c = (StreamChannel) o;
            return c.streamURL.equals(streamURL);
        }
        return false;
    }

    /**
     * Gives the channel hashcode.
     *
     * @return hashCode the channel hashcode
     */
    @Override
    public int hashCode() {
        return name.hashCode();
    }

}
