/*
 * Copyright (C) 2006-2009 the VideoLAN team
 *
 * This file is part of VLMa.
 *
 * VLMa is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * VLMa is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with VLMa. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package org.videolan.vlma.model;

import java.io.Serializable;
import java.net.InetAddress;
import java.util.Date;


/**
 * Basic implementation of Program which represents a programmation.
 *
 * @author Sylvain Cadilhac <sylv at videolan.org>
 * @author Adrien Grand <jpountz at videolan.org>
 */
public final class Program implements Serializable {

    private static final long serialVersionUID = 5L;

    /**
     * The program ID. Used to generate the url at which the program will be
     * accessible. Must be unique.
     */
    private String id;

    /**
     * Runtime field, filled by the OrderManager and used to know which adapter
     * is responsible for streaming this program.
     */
    private volatile Adapter adapter;

    /**
     * Runtime field, filled by the OrderManager and used to know whether this
     * program is effectively broadcasted or not.
     */
    private volatile boolean broadcastState;

    /**
     * The media this program is related to.
     */
    private Media media;

    /**
     * The IP address to use to broadcast, only used with multicast UDP
     * streaming.
     */
    private InetAddress ip;

    /**
     * Time interval in milliseconds between two executions of this program.
     * This number will be taken into account to know wether the program should
     * be played only if <code>liveLength</code> is positive.
     * This number should be positive.
     */
    private long liveInterval;

    /**
     * Length of the execution in milliseconds. A number inferior to 0 means
     * infinity. 0 means that nothing will be broadcasted, whatever the other
     * parameters are.
     */
    private long liveLength;

    /**
     * Number of repetitions of the program. A numer inferior to 0 means
     * infinity.
     */
    private int liveRepetitions;

    /**
     * Time to start broadcasting at.
     */
    private Date liveStart;

    /**
     * Priority of this program.
     */
    private int priority;

    /**
     * Group of this program.
     */
    private String group;

    /**
     * Display name of this program.
     */
    private String name;

    /**
     * Streaming strategy of this program. Must not be null.
     */
    private StreamingStrategy streamingStrategy;

    /**
     * Announcing strategy of this program. null means no announcement.
     */
    private AnnouncingStrategy announcingStrategy;

    /**
     * Transcoding strategy of this program. null means no transcoding.
     */
    private TranscodingStrategy transcodingStrategy;

    public Program() {
        group = null;
        name = "";
        setLiveInterval(0);
        setLiveRepetitions(-1);
        setLiveLength(-1);
        liveStart = new Date();
        setIp(null);
        setPriority(50);
        streamingStrategy = new StreamingStrategy();
        announcingStrategy = null;
        transcodingStrategy = null;
    }

    /**
     * Return the program id.
     *
     * @return the program id
     */
    public String getId() {
        return id;
    }

    /**
     * Set the program id.
     *
     * @param id the id to set
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * Gets adapter name.
     *
     * @return a string containing the adapter name
     */
    public Adapter getAdapter() {
        return adapter;
    }

    /**
     * Gets broadcast state.
     *
     * @return The brocast state :
     *         True if the media is broadcast
     *         False if not
     */
    public boolean getBroadcastState() {
        return broadcastState;
    }

    /**
     * Gets the IP address used to stream the program.
     *
     * @return the IP address
     */
    public InetAddress getIp() {
        return ip;
    }

    /**
     * Gets the repetition interval of the programmation.
     *
     * @return the repetition interval
     */
    public long getLiveInterval() {
        return liveInterval;
    }

    /**
     * Gets the programmation length.
     *
     * @return the programmation length
     */
    public long getLiveLength() {
        return liveLength;
    }

    /**
     * Gets the number of repetitions of the programmation.
     *
     * @return the number of repetitions
     */
    public int getLiveRepetitions() {
        return liveRepetitions;
    }

    /**
     * Gets the programmation start time.
     *
     * @return the start time
     */
    public Date getLiveStart() {
        return new Date(liveStart.getTime());
    }

    /**
     * Gets the IP address of the server which streams on the program IP
     * address.
     *
     * @return the server IP address
     */
    public Server getPlayer() {
        return adapter == null ? null : adapter.getServer();
    }

    /**
     * Gets the programmation priority.
     *
     * @return the programmation priority
     */
    public int getPriority() {
        return priority;
    }

    /**
     * Gets the SAP group.
     *
     * @return the sap group
     */
    public String getGroup() {
        return group;
    }

    /**
     * Gets the SAP name which will be used during the streaming.
     *
     * @return the SAP name
     */
    public String getName() {
        return name;
    }

    /**
     * Gets the program streamingStrategy.
     *
     * @return the streamingStrategy
     */
    public StreamingStrategy getStreamingStrategy() {
        return streamingStrategy;
    }

    /**
     * Gets the program announcing strategy.
     *
     * @return the announcingStrategy
     */
    public AnnouncingStrategy getAnnouncingStrategy() {
        return announcingStrategy;
    }

    /**
     * Gets the program transcoding strategy.
     *
     * @return the transcoding strategy
     */
    public TranscodingStrategy getTranscodingStrategy() {
        return transcodingStrategy;
    }

    /**
     * Gets the media associated to this program.
     *
     * @return the media this program is associated to
     */
    public Media getMedia() {
        return media;
    }

    @Override
    public boolean equals(Object o) {
        if (o == null || !(o instanceof Program))
            return false;
        Program other = (Program)o;
        return media.equals(other.getMedia()) &&
                streamingStrategy == getStreamingStrategy() &&
                (group == null || group.equals(other.getGroup())) &&
                name.equals(other.getName());
    }

    @Override
    public int hashCode() {
        return media.hashCode() + streamingStrategy.hashCode() +
                (group == null ? 0 : group.hashCode()) + name.hashCode();
    }

    /**
     * Returns true if diffusion has to take place.
     *
     * @return true if diffusion has to take place; false otherwise
     */
    public boolean isTimeToPlay() {
        long now = System.currentTimeMillis();
        long startTime = liveStart.getTime();

        return (now - startTime >= 0) &&
                   (liveLength < 0 || now - startTime <= liveLength ||
                       (liveLength > 0 && liveInterval > 0 &&
                           (now - startTime) % liveInterval <= liveLength &&
                           (liveRepetitions < 0 || (liveRepetitions > 0 &&
                               (now - startTime) / liveInterval <= liveRepetitions))));
    }

    /**
     * Sets the adapter name.
     *
     * @param a the adapter in charge of this program
     */
    public void setAdapter(Adapter a) {
        adapter = a;
    }

    /**
     * Tells whether the media of the order is really broadcasted.
     *
     * @param broadcastState true if the media is broadcasted, false otherwise
     */
    public void setBroadcastState(boolean broadcastState) {
        this.broadcastState = broadcastState;
    }

    /**
     * Sets the IP address used to stream the program.
     *
     * @param ip
     *            the IP address to set
     */
    public void setIp(InetAddress ip) {
        this.ip = ip;
    }

    /**
     * Sets the repetition interval of the programmation.
     *
     * @param liveInterval
     *            the repetition interval
     */
    public void setLiveInterval(long liveInterval) {
        this.liveInterval = liveInterval;
    }

    /**
     * Sets the programmation length.
     *
     * @param liveLength
     *            the programmation length
     */
    public void setLiveLength(long liveLength) {
        this.liveLength = liveLength;
    }

    /**
     * Sets the number of repetitions of the programmation.
     *
     * @param liveRepetitions the number of repetitions
     *
     */
    public void setLiveRepetitions(int liveRepetitions) {
        this.liveRepetitions = liveRepetitions;
    }

    /**
     * Sets the programmation start time.
     *
     * @param liveStart
     *            the start time to set
     */
    public void setLiveStart(Date liveStart) {
        this.liveStart = new Date(liveStart.getTime());
    }

    /**
     * Sets the programmation priority.
     *
     * @param priority
     *            the priority to set
     */
    public void setPriority(int priority) {
        this.priority = priority;
    }

    /**
     * Sets the group.
     *
     * @param group
     *            the group to set
     */
    public void setGroup(String group) {
        this.group = group;
    }

    /**
     * Sets the name which will be used for announcements.
     *
     * @param name
     *            the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Sets the streamingStrategy.
     *
     * @param streamingStrategy the streamingStrategy to set
     */
    public void setStreamingStrategy(StreamingStrategy streamingStrategy) {
        this.streamingStrategy = streamingStrategy;
    }

    /**
     * Sets the announcing strategy.
     *
     * @param announcingStrategy the announcingStrategy to set
     */
    public void setAnnouncingStrategy(AnnouncingStrategy announcingStrategy) {
        this.announcingStrategy = announcingStrategy;
    }

    /**
     * Sets the transcoding strategy.
     *
     * @param transcodingStrategy the transcodingStrategy to set
     */
    public void setTranscodingStrategy(TranscodingStrategy transcodingStrategy) {
        this.transcodingStrategy = transcodingStrategy;
    }

    /**
     * Sets the program media.
     *
     * @param media the media to set
     */
    public void setMedia(Media media) {
        this.media = media;
    }

    /**
     * Check wether this program can be added to the supplied group.
     *
     * @param group the group to check
     * @return true if, and only if the program can be added
     */
    public boolean belongsToGroup(ProgramGroup group) {
        return group.isAddable(this);
    }

}
