/*
 * Copyright (C) 2008 the VideoLAN team
 *
 * This file is part of VLMa.
 *
 * VLMa is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * VLMa is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with VLMa. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package org.videolan.vlma.web.tag;

import java.io.IOException;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.jstl.fmt.LocaleSupport;

import org.apache.log4j.Logger;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.servlet.tags.RequestContextAwareTag;
import org.videolan.vlma.Data;

public class Program extends RequestContextAwareTag {

    private static final long serialVersionUID = 2L;
    private static final Logger logger = Logger.getLogger(Program.class);
    private static final String NOT_RUNNING = "medias.list.status.programmed-nonrunning";
    private static final String RUNNING = "medias.list.status.programmed-running";
    private static final String ASSIGNED = "medias.list.status.programmed-assigned";

    private Data data;
    private org.videolan.vlma.model.Program program;

    public void setProgram(org.videolan.vlma.model.Program program) {
        this.program = program;
    }

    public int doStartTagInternal() throws JspException {
        WebApplicationContext webAppContext = getRequestContext().getWebApplicationContext();
        data = (Data) webAppContext.getBean("dataImporter");
        Object[] params;
        JspWriter out = pageContext.getOut();
        try {
            if (program.getGroup() != null) {
                out.print("<span class=\"program sap group\">");
                out.print(program.getGroup());
                out.print("</span> / ");
            }
            out.print("<span class=\"program sap name\">");
            out.print(program.getName());
            out.print("</span>");
            out.println(": ");
            out.print(program.getStreamingStrategy().getType().getName());
            out.println(" ");
            if (program.getPlayer() == null) {
                params = new Object[]{ Integer.valueOf(program.getPriority()) };
                out.print(LocaleSupport.getLocalizedMessage(pageContext, NOT_RUNNING, params));
            } else {
                out.print("(<a href=\"");
                String url = data.getUrl(program);
                out.print(url);
                out.print("\">");
                out.print(url);
                out.print("</a>) ");
                if (program.getBroadcastState()) {
                    params = new Object[]{ program.getPriority(), program.getPlayer().getName() };
                    out.print(LocaleSupport.getLocalizedMessage(pageContext, RUNNING, params));
                } else {
                    params = new Object[]{ program.getPriority(), program.getPlayer().getName() };
                    out.print(LocaleSupport.getLocalizedMessage(pageContext, ASSIGNED, params));
                }
            }
        } catch(IOException e) {
            logger.error(e);
        }
        return SKIP_BODY;
    }

}
