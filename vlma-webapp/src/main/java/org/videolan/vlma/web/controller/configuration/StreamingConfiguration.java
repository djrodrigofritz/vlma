/*
 * Copyright (C) 2008 the VideoLAN team
 *
 * This file is part of VLMa.
 *
 * VLMa is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * VLMa is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with VLMa. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package org.videolan.vlma.web.controller.configuration;

public class StreamingConfiguration {

    static final String[] monitors = { "NONE", "SNMP", "HTTP" };

    private Integer vlc_telnet_port;
    private String vlc_telnet_password;
    private String vlc_monitor_impl;
    private String vlc_snmp_community;
    private String vlc_snmp_oid_cpu_load;
    private String vlc_snmp_oid_vlc_cpu;
    private String vlc_snmp_oid_vlc_mem;
    private String vlc_snmp_oid_traffic_in;
    private String vlc_snmp_oid_traffic_out;
    private String vlc_snmp_oid_vlc_version;
    private String vlc_snmp_oid_vlc_restart;
    private String vlc_snmp_oid_vlc_logtail;
    private String vlc_snmp_oid_vlc_uptime;
    private Integer vlc_monitor_http_port;
    private String vlc_monitor_http_login;
    private String vlc_monitor_http_password;
    private String vlma_streaming;
    private String vlma_encapsulation;
    private Integer vlma_streaming_http_port;
    private String vlma_streaming_udp_ipbank_min;
    private String vlma_streaming_udp_ipbank_max;
    private Integer vlc_stream_ttl;
    private Integer vlc_stream_dvb_bandwidth;

    public Integer getVlc_monitor_http_port() {
        return vlc_monitor_http_port;
    }
    public void setVlc_monitor_http_port(Integer vlc_monitor_http_port) {
        this.vlc_monitor_http_port = vlc_monitor_http_port;
    }
    public String getVlc_monitor_http_login() {
        return vlc_monitor_http_login;
    }
    public void setVlc_monitor_http_login(String vlc_monitor_http_login) {
        this.vlc_monitor_http_login = vlc_monitor_http_login;
    }
    public String getVlc_monitor_http_password() {
        return vlc_monitor_http_password;
    }
    public void setVlc_monitor_http_password(String vlc_monitor_http_password) {
        this.vlc_monitor_http_password = vlc_monitor_http_password;
    }
    public Integer getVlc_stream_dvb_bandwidth() {
        return vlc_stream_dvb_bandwidth;
    }
    public void setVlc_stream_dvb_bandwidth(Integer vlc_stream_dvb_bandwidth) {
        this.vlc_stream_dvb_bandwidth = vlc_stream_dvb_bandwidth;
    }
    public Integer getVlc_stream_ttl() {
        return vlc_stream_ttl;
    }
    public void setVlc_stream_ttl(Integer vlc_stream_ttl) {
        this.vlc_stream_ttl = vlc_stream_ttl;
    }
    public String getVlc_telnet_password() {
        return vlc_telnet_password;
    }
    public void setVlc_telnet_password(String vlc_telnet_password) {
        this.vlc_telnet_password = vlc_telnet_password;
    }
    public Integer getVlc_telnet_port() {
        return vlc_telnet_port;
    }
    public void setVlc_telnet_port(Integer vlc_telnet_port) {
        this.vlc_telnet_port = vlc_telnet_port;
    }
    public String getVlma_encapsulation() {
        return vlma_encapsulation;
    }
    public void setVlma_encapsulation(String vlma_encapsulation) {
        this.vlma_encapsulation = vlma_encapsulation;
    }
    public String getVlma_streaming() {
        return vlma_streaming;
    }
    public void setVlma_streaming(String vlma_streaming) {
        this.vlma_streaming = vlma_streaming;
    }
    public Integer getVlma_streaming_http_port() {
        return vlma_streaming_http_port;
    }
    public void setVlma_streaming_http_port(Integer vlma_streaming_http_port) {
        this.vlma_streaming_http_port = vlma_streaming_http_port;
    }
    public String getVlma_streaming_udp_ipbank_max() {
        return vlma_streaming_udp_ipbank_max;
    }
    public void setVlma_streaming_udp_ipbank_max(String vlma_streaming_udp_ipbank_max) {
        this.vlma_streaming_udp_ipbank_max = vlma_streaming_udp_ipbank_max;
    }
    public String getVlma_streaming_udp_ipbank_min() {
        return vlma_streaming_udp_ipbank_min;
    }
    public void setVlma_streaming_udp_ipbank_min(String vlma_streaming_udp_ipbank_min) {
        this.vlma_streaming_udp_ipbank_min = vlma_streaming_udp_ipbank_min;
    }
    public String getVlc_snmp_oid_cpu_load() {
        return vlc_snmp_oid_cpu_load;
    }
    public void setVlc_snmp_oid_cpu_load(String vlc_snmp_oid_cpu_load) {
        this.vlc_snmp_oid_cpu_load = vlc_snmp_oid_cpu_load;
    }
    public String getVlc_snmp_oid_traffic_in() {
        return vlc_snmp_oid_traffic_in;
    }
    public void setVlc_snmp_oid_traffic_in(String vlc_snmp_oid_traffic_in) {
        this.vlc_snmp_oid_traffic_in = vlc_snmp_oid_traffic_in;
    }
    public String getVlc_snmp_oid_traffic_out() {
        return vlc_snmp_oid_traffic_out;
    }
    public void setVlc_snmp_oid_traffic_out(String vlc_snmp_oid_traffic_out) {
        this.vlc_snmp_oid_traffic_out = vlc_snmp_oid_traffic_out;
    }
    public String getVlc_snmp_oid_vlc_cpu() {
        return vlc_snmp_oid_vlc_cpu;
    }
    public void setVlc_snmp_oid_vlc_cpu(String vlc_snmp_oid_vlc_cpu) {
        this.vlc_snmp_oid_vlc_cpu = vlc_snmp_oid_vlc_cpu;
    }
    public String getVlc_snmp_oid_vlc_logtail() {
        return vlc_snmp_oid_vlc_logtail;
    }
    public void setVlc_snmp_oid_vlc_logtail(String vlc_snmp_oid_vlc_logtail) {
        this.vlc_snmp_oid_vlc_logtail = vlc_snmp_oid_vlc_logtail;
    }
    public String getVlc_snmp_oid_vlc_mem() {
        return vlc_snmp_oid_vlc_mem;
    }
    public void setVlc_snmp_oid_vlc_mem(String vlc_snmp_oid_vlc_mem) {
        this.vlc_snmp_oid_vlc_mem = vlc_snmp_oid_vlc_mem;
    }
    public String getVlc_snmp_oid_vlc_restart() {
        return vlc_snmp_oid_vlc_restart;
    }
    public void setVlc_snmp_oid_vlc_restart(String vlc_snmp_oid_vlc_restart) {
        this.vlc_snmp_oid_vlc_restart = vlc_snmp_oid_vlc_restart;
    }
    public String getVlc_snmp_oid_vlc_uptime() {
        return vlc_snmp_oid_vlc_uptime;
    }
    public void setVlc_snmp_oid_vlc_uptime(String vlc_snmp_oid_vlc_uptime) {
        this.vlc_snmp_oid_vlc_uptime = vlc_snmp_oid_vlc_uptime;
    }
    public String getVlc_snmp_oid_vlc_version() {
        return vlc_snmp_oid_vlc_version;
    }
    public void setVlc_snmp_oid_vlc_version(String vlc_snmp_oid_vlc_version) {
        this.vlc_snmp_oid_vlc_version = vlc_snmp_oid_vlc_version;
    }
    public String getVlc_monitor_impl() {
        return vlc_monitor_impl;
    }
    public void setVlc_monitor_impl(String vlc_monitor_impl) {
        this.vlc_monitor_impl = vlc_monitor_impl;
    }
    public String getVlc_snmp_community() {
        return vlc_snmp_community;
    }
    public void setVlc_snmp_community(String vlc_snmp_community) {
        this.vlc_snmp_community = vlc_snmp_community;
    }

}
