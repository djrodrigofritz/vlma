/*
 * Copyright (C) 2006-2008 the VideoLAN team
 *
 * This file is part of VLMa.
 *
 * VLMa is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * VLMa is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with VLMa. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package org.videolan.vlma.web.controller.media;

import java.util.HashSet;
import java.util.Set;

import org.videolan.vlma.model.StreamingStrategy.Type;
import org.videolan.vlma.model.StreamingStrategy.Protocol;
import org.videolan.vlma.model.StreamingStrategy.Encapsulation;
import org.videolan.vlma.model.AnnouncingStrategy.Announcement;
import org.videolan.vlma.model.TranscodingStrategy.AudioCodec;
import org.videolan.vlma.model.TranscodingStrategy.VideoCodec;

public class MediaProgramAdd {

    private String id;

    /* SAP name and group */
    private String sap;
    private String group;

    private String ip;

    private String priority;

    private Type type;
    private Type[] types;

    private Protocol protocol;

    private Encapsulation mux;

    private boolean transcodeVideo;
    private VideoCodec videoCodec;
    private int videoBitrate;
    private double scale;
    private boolean deinterlace;

    private boolean transcodeAudio;
    private AudioCodec audioCodec;
    private int audioBitrate;

    private AnnouncementCheckbox[] announcements;

    private int mediaId;

    public MediaProgramAdd() {
        announcements = new AnnouncementCheckbox[Announcement.values().length];
        int i = 0;
        for (Announcement announcement : Announcement.values()) {
            AnnouncementCheckbox ac = new AnnouncementCheckbox();
            ac.setAnnouncement(announcement);
            announcements[i++] = ac;
        }
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Type[] getTypes() {
        return types;
    }

    public void setTypes(Type[] types) {
        this.types = types;
    }

    public Protocol[] getProtocols() {
        return Protocol.values();
    }

    public Encapsulation[] getMuxs() {
        return Encapsulation.values();
    }

    public VideoCodec[] getVideoCodecs() {
        return VideoCodec.values();
    }

    public AudioCodec[] getAudioCodecs() {
        return AudioCodec.values();
    }

    public String getPriority() {
        return priority;
    }

    public void setPriority(String priority) {
        this.priority = priority;
    }

    public String getSap() {
        return sap;
    }

    public void setSap(String sap) {
        this.sap = sap;
    }

    public String getGroup() {
        return group;
    }

    public void setGroup(String group) {
        this.group = group;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip.trim();
    }

    public int getMediaId() {
        return mediaId;
    }

    public void setMediaId(int mediaId) {
        this.mediaId = mediaId;
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public Encapsulation getMux() {
        return mux;
    }

    public void setMux(Encapsulation mux) {
        this.mux = mux;
    }

    public Protocol getProtocol() {
        return protocol;
    }

    public void setProtocol(Protocol protocol) {
        this.protocol = protocol;
    }

    public AnnouncementCheckbox[] getAnnouncementCheckboxes() {
        return announcements;
    }

    public void setAnnouncementCheckboxes(AnnouncementCheckbox[] announcements) {
        this.announcements = announcements;
    }

    public Set<Announcement> getAnnouncements() {
        Set<Announcement> result = new HashSet<Announcement>();
        for(AnnouncementCheckbox a : getAnnouncementCheckboxes()) {
            if (a.getChecked())
                result.add(a.getAnnouncement());
        }
        return result;
    }

    public void setAnnouncements(Set<Announcement> announcements) {
        for(AnnouncementCheckbox a : getAnnouncementCheckboxes()) {
            a.setChecked(announcements.contains(a.getAnnouncement()));
        }
    }

    public int getAudioBitrate() {
        return audioBitrate;
    }

    public void setAudioBitrate(int audioBitrate) {
        this.audioBitrate = audioBitrate;
    }

    public AudioCodec getAudioCodec() {
        return audioCodec;
    }

    public void setAudioCodec(AudioCodec audioCodec) {
        this.audioCodec = audioCodec;
    }

    public boolean getDeinterlace() {
        return deinterlace;
    }

    public void setDeinterlace(boolean deinterlace) {
        this.deinterlace = deinterlace;
    }

    public double getScale() {
        return scale;
    }

    public void setScale(double scale) {
        this.scale = scale;
    }

    public boolean getTranscodeAudio() {
        return transcodeAudio;
    }

    public void setTranscodeAudio(boolean transcodeAudio) {
        this.transcodeAudio = transcodeAudio;
    }

    public boolean getTranscodeVideo() {
        return transcodeVideo;
    }

    public void setTranscodeVideo(boolean transcodeVideo) {
        this.transcodeVideo = transcodeVideo;
    }

    public int getVideoBitrate() {
        return videoBitrate;
    }

    public void setVideoBitrate(int videoBitrate) {
        this.videoBitrate = videoBitrate;
    }

    public VideoCodec getVideoCodec() {
        return videoCodec;
    }

    public void setVideoCodec(VideoCodec videoCodec) {
        this.videoCodec = videoCodec;
    }

    public static class AnnouncementCheckbox {

        private boolean checked;

        private Announcement announcement;

        public Announcement getAnnouncement() {
            return announcement;
        }

        public void setAnnouncement(Announcement announcement) {
            this.announcement = announcement;
        }

        public boolean getChecked() {
            return checked;
        }

        public void setChecked(boolean checked) {
            this.checked = checked;
        }

    }

}
