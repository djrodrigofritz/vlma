/*
 * Copyright (C) 2009 the VideoLAN team
 *
 * This file is part of VLMa.
 *
 * VLMa is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * VLMa is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with VLMa. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package org.videolan.vlma.web.util;

import java.io.IOException;
import java.net.URL;
import java.rmi.RemoteException;
import java.util.List;

import org.springframework.remoting.rmi.RmiProxyFactoryBean;
import org.videolan.vlma.Data;
import org.videolan.vlma.exception.AlreadyExistsException;
import org.videolan.vlma.exception.NotFoundException;
import org.videolan.vlma.model.Command;
import org.videolan.vlma.model.Media;
import org.videolan.vlma.model.Program;
import org.videolan.vlma.model.Satellite;
import org.videolan.vlma.model.Server;

/**
 * A proxy to the remote Data implementation in the daemon.
 *
 * We don't use Spring's RmiProxyFactoryBean directly because the constructor
 * throws a RemoteLookupFailureException when the remote service is not
 * available, preventing us from initializing the webapp. So we need to add
 * one more level of indirection.
 *
 * @author Adrien Grand
 */
public class DataProxy implements Data {

    private static final String SERVICE_URL = "rmi://localhost:9050/VLMaDataService";

    private Data data;

    public DataProxy() {
        data = null;
    }

    private synchronized Data getData() {
        if (data == null) {
            RmiProxyFactoryBean factory = new RmiProxyFactoryBean();
            factory.setServiceInterface(Data.class);
            factory.setServiceUrl(SERVICE_URL);
            factory.afterPropertiesSet();
            factory.setRefreshStubOnConnectFailure(true);
            data = (Data) factory.getObject();
        }
        return data;
    }

    public void add(Satellite arg0) throws AlreadyExistsException, RemoteException {
        getData().add(arg0);
    }

    public void add(Server arg0) throws AlreadyExistsException, RemoteException {
        getData().add(arg0);
    }

    public void add(Media arg0) throws RemoteException {
        getData().add(arg0);
    }

    public void clearProperty(String arg0) throws RemoteException {
        getData().clearProperty(arg0);
    }

    public List<Command> getCommands() throws RemoteException {
        return getData().getCommands();
    }

    public Integer getInt(String arg0) throws RemoteException {
        return getData().getInt(arg0);
    }

    public List<String> getList(String arg0) throws RemoteException {
        return getData().getList(arg0);
    }

    public Media getMedia(int arg0) throws RemoteException {
        return getData().getMedia(arg0);
    }

    public List<Media> getMedias() throws RemoteException {
        return getData().getMedias();
    }

    public Satellite getSatellite(int arg0) throws NotFoundException, RemoteException {
        return getData().getSatellite(arg0);
    }

    public List<Satellite> getSatellites() throws RemoteException {
        return getData().getSatellites();
    }

    public Server getServer(int arg0) throws NotFoundException, RemoteException {
        return getData().getServer(arg0);
    }

    public List<Server> getServers() throws RemoteException {
        return getData().getServers();
    }

    public String getString(String arg0) throws RemoteException {
        return getData().getString(arg0);
    }

    public String getUrl(Program arg0) throws RemoteException {
        return getData().getUrl(arg0);
    }

    public List<String> getVlcLogTail(Server arg0) throws RemoteException, IOException {
        return getData().getVlcLogTail(arg0);
    }

    public String getVlcResponse(Server arg0, String arg1) throws RemoteException, IOException {
        return getData().getVlcResponse(arg0, arg1);
    }

    public Long getVlcUptime(Server arg0) throws RemoteException, IOException {
        return getData().getVlcUptime(arg0);
    }

    public String getVlcVersion(Server arg0) throws RemoteException, IOException {
        return getData().getVlcVersion(arg0);
    }

    public void giveOrders() throws RemoteException {
        getData().giveOrders();
    }

    public Program newProgram() throws RemoteException {
        return getData().newProgram();
    }

    public void reload() throws RemoteException {
        getData().reload();
    }

    public void remove(Satellite arg0) throws NotFoundException, RemoteException {
        getData().remove(arg0);
    }

    public void remove(Server arg0) throws NotFoundException, RemoteException {
        getData().remove(arg0);
    }

    public void remove(Media arg0) throws NotFoundException, RemoteException {
        getData().remove(arg0);
    }

    public void restartVlc(Server arg0) throws RemoteException, IOException {
        getData().restartVlc(arg0);
    }

    public void setProperty(String arg0, Object arg1) throws RemoteException {
        getData().setProperty(arg0, arg1);
    }

    public void startCheckAllVLCs() throws RemoteException {
        getData().startCheckAllVLCs();
    }

    public void stop() throws RemoteException {
        getData().stop();
    }

    public void update(Satellite arg0) throws NotFoundException, RemoteException {
        getData().update(arg0);
    }

    public void update(Server arg0) throws NotFoundException, RemoteException {
        getData().update(arg0);
    }

    public void update(Media arg0) throws NotFoundException, RemoteException {
        getData().update(arg0);
    }

    public void updateSatChannels(URL arg0) throws IOException, RemoteException {
        getData().updateSatChannels(arg0);
    }

}
