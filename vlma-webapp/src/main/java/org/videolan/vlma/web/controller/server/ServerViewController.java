/*
 * Copyright (C) 2006-2008 the VideoLAN team
 *
 * This file is part of VLMa.
 *
 * VLMa is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * VLMa is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with VLMa. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package org.videolan.vlma.web.controller.server;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;
import org.videolan.vlma.Data;
import org.videolan.vlma.model.Server;

public class ServerViewController implements Controller {

    private static final Logger logger = Logger.getLogger(ServerViewController.class);

    public ModelAndView handleRequest(HttpServletRequest arg0,
            HttpServletResponse arg1) throws Exception {
        Server server = data.getServer(Integer.parseInt(arg0
                .getParameter("server")));
        ModelAndView mav = new ModelAndView();
        mav.addObject("server", server);
        try {
            String vlcVersion = data.getVlcVersion(server);
            mav.addObject("vlcVersion", vlcVersion);
        } catch(IOException e) {
            mav.addObject("vlcVersion", null);
            logger.error("Cannot retrieve VLC version", e);
        }
        try {
            List<String> logs = data.getVlcLogTail(server);
            mav.addObject("vlcLogs", logs);
        } catch(IOException e) {
            mav.addObject("vlcLogs", null);
            logger.error("Cannot retrieve VLC logs", e);
        }
        try {
            Long uptime = data.getVlcUptime(server);
            if(uptime != null)
                mav.addObject("vlcUptime", new TimeSpan(uptime));
            else
                mav.addObject("vlcUptime", null);
        } catch(IOException e) {
            mav.addObject("vlcUptime", null);
            logger.error("Cannot retrieve VLC uptime", e);
        }
        Boolean graphsEnabled = !"NONE".equals(data.getString("vlc.monitor.impl"));
        mav.addObject("graphsEnabled", graphsEnabled);
        return mav;
    }

    private Data data;

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public static class TimeSpan {

        private long millis;
        private static final String TIME_FORMAT = "HH:mm:ss";

        public TimeSpan(long millis) {
            this.millis = millis;
        }

        public long getDays() {
            return millis / (1000 * 60 * 60 * 24);
        }

        public String getHours() {
            long hoursMillis = millis % (1000 * 60 * 60 * 24);
            Date date = new Date(hoursMillis);
            // Don't store the dateformat in a static field because
            // DateFormat is not thread safe
            return new SimpleDateFormat(TIME_FORMAT).format(date);
        }
    }

}
