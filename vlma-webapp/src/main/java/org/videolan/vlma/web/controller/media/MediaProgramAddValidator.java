/*
 * Copyright (C) 2006-2008 the VideoLAN team
 *
 * This file is part of VLMa.
 *
 * VLMa is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * VLMa is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with VLMa. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package org.videolan.vlma.web.controller.media;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.rmi.RemoteException;

import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import org.videolan.vlma.Data;
import org.videolan.vlma.model.Media;
import org.videolan.vlma.model.Program;

public class MediaProgramAddValidator implements Validator {

    private Data data;

    @SuppressWarnings("unchecked")
    public boolean supports(Class arg0) {
        return arg0.equals(MediaProgramAdd.class);
    }

    public void validate(Object arg0, Errors arg1) {
        MediaProgramAdd mediasProgramAdd = (MediaProgramAdd) arg0;

        if (mediasProgramAdd == null) {
            arg1.rejectValue("sap", "medias.program.add.error.not-specified");
            return;
        } else {
            try {
                Media media = data.getMedia(mediasProgramAdd.getMediaId());
                if (media == null) {
                    arg1.rejectValue("sap", "medias.program.add.error.nonexisting");
                    return;
                }
            } catch (RemoteException e1) {
                arg1.rejectValue("sap", "error.remote_exception");
            }
            if ("".equals(mediasProgramAdd.getSap())) {
                arg1.rejectValue("sap", "medias.program.add.error.invalidsap");
            }
            if ("".equals(mediasProgramAdd.getId())) {
                arg1.rejectValue("id", "medias.program.add.error.emptyid");
            }
            if (mediasProgramAdd.getId().matches(".*[^a-zA-Z0-9]+.*")) {
                arg1.rejectValue("id", "medias.program.add.error.nonalphanumid");
            }
            try {
                boolean found = false;
                for (Media media : data.getMedias()) {
                    for (Program program : media.getPrograms()) {
                        if (program.getId().equals(mediasProgramAdd.getId())) {
                            arg1.rejectValue("id", "medias.program.add.error.idalreadyinuse");
                            found = true;
                            break;
                        }
                    }
                    if (found) break;
                }
            } catch (RemoteException e1) {
                arg1.rejectValue("id", "error.remote_exception");
            }
            try {
                int p = Integer.parseInt(mediasProgramAdd.getPriority());
                if (p < 1) {
                    arg1.rejectValue("priority",
                            "medias.program.add.error.invalidpriority");
                }
            } catch (NumberFormatException e) {
                arg1.rejectValue("priority",
                        "medias.program.add.error.invalidpriority");
            }
            String ip = mediasProgramAdd.getIp();
            if (ip!= null && ip.length() > 0) {
                try {
                    InetAddress.getByName(ip);
                } catch (UnknownHostException e) {
                    arg1.rejectValue("ip", "medias.program.add.error.invalidip");
                }
            }
            if (mediasProgramAdd.getScale() <= 0) {
                arg1.rejectValue("ip", "medias.program.add.error.negativescale");
            }
            if (mediasProgramAdd.getVideoBitrate() <= 0) {
                arg1.rejectValue("ip", "medias.program.add.error.negativevb");
            }
            if (mediasProgramAdd.getAudioBitrate() <= 0) {
                arg1.rejectValue("ip", "medias.program.add.error.negativeab");
            }
        }

    }

    /**
     * @param data the data to set
     */
    public void setData(Data data) {
        this.data = data;
    }

}
