/*
 * Copyright (C) 2006-2008 the VideoLAN team
 *
 * This file is part of VLMa.
 *
 * VLMa is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * VLMa is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with VLMa. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package org.videolan.vlma.web.controller.media.stream;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;
import org.springframework.web.servlet.view.RedirectView;
import org.videolan.vlma.Data;
import org.videolan.vlma.model.Media;
import org.videolan.vlma.model.Server;
import org.videolan.vlma.model.StreamChannel;

public class StreamChannelEditController extends SimpleFormController {

    private Data data;

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    @Override
    public ModelAndView onSubmit(Object command) throws Exception {
        int id = ((StreamChannelAdd)command).getId();

        StreamChannel streamChannel = (StreamChannel)data.getMedia(id);
        streamChannel.setStreamURL(((StreamChannelAdd) command).getStreamURL().trim());
        Integer serverId;
        try {
            serverId = Integer.valueOf(((StreamChannelAdd) command).getServer());
        } catch (NumberFormatException e) {
            serverId = null;
        }
        streamChannel.setServer(serverId == null ? null : data.getServer(serverId));
        data.update(streamChannel);

        return new ModelAndView(new RedirectView(getSuccessView()));
    }

    @Override
    protected Object formBackingObject(HttpServletRequest request) throws Exception {
        int id = Integer.parseInt(request.getParameter("Id"));
        Media media = data.getMedia(id);

        StreamChannelAdd streamChannelsAdd = new StreamChannelAdd();
        streamChannelsAdd.setId(id);
        streamChannelsAdd.setName(media.getName());
        streamChannelsAdd.setStreamURL(((StreamChannel)media).getStreamURL());
        Server server = ((StreamChannel)media).getServer();
        streamChannelsAdd.setServer(server == null ? null : String.valueOf(server.getId()));
        streamChannelsAdd.setServers(data.getServers());

        return streamChannelsAdd;
    }

    @Override
    protected ModelAndView handleRequestInternal(HttpServletRequest arg0,
            HttpServletResponse arg1) throws Exception {
        return super.handleRequestInternal(arg0, arg1);
    }

}
