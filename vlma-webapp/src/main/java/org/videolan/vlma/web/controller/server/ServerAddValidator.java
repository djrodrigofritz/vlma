/*
 * Copyright (C) 2006-2008 the VideoLAN team
 *
 * This file is part of VLMa.
 *
 * VLMa is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * VLMa is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with VLMa. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package org.videolan.vlma.web.controller.server;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.rmi.RemoteException;
import java.util.List;

import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import org.videolan.vlma.Data;
import org.videolan.vlma.model.Server;

public class ServerAddValidator implements Validator {

    private Data data;

    @SuppressWarnings("unchecked")
    public boolean supports(Class arg0) {
        return arg0.equals(ServerAdd.class);
    }

    private static boolean validateName(String s) {
        if (s == null) return false;
        int index = 0;
        while (index < s.length()) {
            char c = s.charAt(index);
            if ((c >= 'a' && c <= 'z') ||
                (c >= 'A' && c <= 'Z') ||
                (c >= '0' && c <= '9') ||
                (c == '-' || c == '_')) {
                index++;
            } else {
                return false;
            }
        }
        return true;
    }

    public void validate(Object arg0, Errors arg1) {
        ServerAdd serversAdd = (ServerAdd) arg0;

        if (serversAdd == null) {
            arg1.rejectValue("name", "servers.add.error.not-specified");
            return;
        } else {
            if (!validateName(serversAdd.getName())) {
                arg1.rejectValue("name", "servers.add.error.invalidname");
                return;
            }
            InetAddress serverAddress;
            if ("".equals(serversAdd.getAddress())) {
                arg1.rejectValue("address", "servers.add.error.invalidaddress");
                return;
            }
            try {
                serverAddress = InetAddress.getByName(serversAdd.getAddress());
            } catch (UnknownHostException e) {
                arg1.rejectValue("address", "servers.add.error.invalidaddress");
                return;
            }
            List<Server> servers;
            try {
                servers = data.getServers();
                if (servers != null) {
                    for (Server server : servers) {
                        if (server.getName().equals(serversAdd.getName())) {
                            arg1.rejectValue("name", "servers.add.error.existingname");
                            return;
                        }
                        try {
                            if (serverAddress.equals(server.getIp())) {
                                arg1.rejectValue("address",
                                        "servers.add.error.existingaddress");
                                return;
                            }
                        } catch (UnknownHostException e) {
                            // Should we reject in case of a DNS failure?
                        }
                    }
                }
            } catch (RemoteException e) {
                arg1.rejectValue("address", "error.Remote_exception");
            }
        }
    }

    /**
     * @param data the data to set
     */
    public void setData(Data data) {
        this.data = data;
    }

}
