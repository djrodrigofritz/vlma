<%@ include file="/WEB-INF/jsp/include.jsp" %>

<h1><fmt:message key="orders.interactive.page" /></h1>

<c:choose>
    <c:when test="${empty servers}">
        <p><fmt:message key="orders.interactive.noserver" /></p>
    </c:when>
    <c:otherwise>
        <table class="fullwidth">
            <tr>
                <td style="width: 50px"><fmt:message key="orders.interactive.server" /></td>
                <td>
	                <select id="serverSelect">
	                    <c:forEach items="${servers}" var="server">
	                        <option value="<c:out value="${server.id}" />"><c:out value="${server.name}" /></option>
	                    </c:forEach>
	                </select>
	            </td>
            </tr>
            <tr>
                <td style="width: 30px"><fmt:message key="orders.interactive.command" /></td>
                <td><input type="text" class="text" id="command" style="width: 100%" /></td>
                <td style="width:30px"><input type="button" onclick="updateResponse()" value="Go!" /></td>
            </tr>
            <tr>
                <td><fmt:message key="orders.interactive.response" /></td>
                <td colspan="2"><pre><span id="response" /></pre></td>
            </tr>
        </table>
    </c:otherwise>
</c:choose>

<div class="help">
    <table>
        <tr><td style="width: 30px">
            <img src="<c:url value="/img/help.png" />" alt="Help" />
        </td><td>
            <fmt:message key="orders.interactive.help" />
        </td></tr>
    </table>
</div>

<script type="text/javascript">
function updateResponse() {
    AjaxData.getVlcResponse(
        dwr.util.getValue("serverSelect"),
        dwr.util.getValue("command"),
        function(data) {
            dwr.util.setValue("response", data);
        }
    );
}
</script>