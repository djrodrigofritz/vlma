<%@ include file="/WEB-INF/jsp/include.jsp"%>

<h1>
    <fmt:message key="medias.program.adding">
        <fmt:param value="${media.name}" />
    </fmt:message>
</h1>

<form method="post">
<table>
    <tr>
        <td align="right"><fmt:message key="medias.program.add.id" /></td>
        <spring:bind path="mediaProgramAdd.id">
            <td><input type="text" name="id" value="<c:out value="${status.value}" />" /></td>
            <td><span class="error"><c:out value="${status.errorMessage}" /></span></td>
        </spring:bind>
    </tr>
    <tr>
        <td align="right"><fmt:message key="medias.program.add.sap" /></td>
        <spring:bind path="mediaProgramAdd.sap">
            <td><input type="text" name="sap" value="<c:out
                value="${status.value}" />" /></td>
            <td><span class="error"><c:out
                value="${status.errorMessage}" /></span></td>
        </spring:bind>
    </tr>
    <tr>
        <td align="right"><fmt:message key="medias.program.add.group" /></td>
        <spring:bind path="mediaProgramAdd.group">
            <td><input type="text" name="group" value="<c:out
                value="${status.value}" />" /></td>
            <td><span class="error"><c:out
                value="${status.errorMessage}" /></span></td>
        </spring:bind>
    </tr>
    <tr>
        <td align="right"><fmt:message key="medias.program.add.priority" /></td>
        <spring:bind path="mediaProgramAdd.priority">
            <td><input type="text" name="priority"
                value="<c:out value="${status.value}"/>"></td>
            <td><span class="error"><c:out
                value="${status.errorMessage}" /></span></td>
        </spring:bind>
    </tr>
    <tr>
        <td align="right"><fmt:message key="medias.program.add.type" /></td>
        <spring:bind path="mediaProgramAdd.type">
            <td><select id="type" name="type" onchange="onTypeUpdate();">
                <c:forEach items="${mediaProgramAdd.types}" var="typeOption">
                    <option value="${typeOption}" <c:if test="${mediaProgramAdd.type == typeOption}">selected="true"</c:if>>
                        <c:out value="${typeOption.name}" />
                    </option>
                </c:forEach>
            </select></td>
            <td><span class="error"><c:out value="${status.errorMessage}" /></span></td>
        </spring:bind>
    </tr>
    <tr>
        <td align="right"><fmt:message key="medias.program.add.protocol" /></td>
        <spring:bind path="mediaProgramAdd.protocol">
            <td><select name="protocol" id="protocol" onchange="onProtocolUpdate();">
                <c:forEach items="${mediaProgramAdd.protocols}" var="protocolOption">
                    <option value="${protocolOption}" <c:if test="${mediaProgramAdd.protocol == protocolOption}">selected="true"</c:if>>
                        <c:out value="${protocolOption.name}" />
                    </option>
                </c:forEach>
            </select></td>
            <td><span class="error"><c:out value="${status.errorMessage}" /></span></td>
        </spring:bind>
    </tr>
    <tr id="ipTr">
        <td align="right"><fmt:message key="medias.program.add.ip" /></td>
        <spring:bind path="mediaProgramAdd.ip">
            <td><input type="text" name="ip" value="<c:out value="${status.value}" />" /></td>
            <td><span class="error"><c:out value="${status.errorMessage}" /></span></td>
        </spring:bind>
    </tr>
    <tr>
        <td align="right"><fmt:message key="medias.program.add.mux" /></td>
        <spring:bind path="mediaProgramAdd.mux">
            <td><select id="mux" name="mux" onchange="onMuxUpdate();">
                <c:forEach items="${mediaProgramAdd.muxs}" var="muxOption">
                    <option value="${muxOption}" <c:if test="${mediaProgramAdd.mux == muxOption}">selected="true"</c:if>>
                        <c:out value="${muxOption.name}" />
                    </option>
                </c:forEach>
            </select></td>
            <td><span class="error"><c:out value="${status.errorMessage}" /></span></td>
        </spring:bind>
    </tr>
    <tr>
        <td align="right"><fmt:message key="medias.program.add.transcodevideo" /></td>
        <spring:bind path="mediaProgramAdd.transcodeVideo">
            <td><input id="transcodeVideo" type="checkbox" name="transcodeVideo" onchange="onTranscodeVideoUpdate(this.checked);" /></td>
            <td><span class="error"><c:out value="${status.errorMessage}" /></span></td>
        </spring:bind>
    </tr>
    <tr class="videoStuff">
        <td align="right"><fmt:message key="medias.program.add.vcodec" /></td>
        <spring:bind path="mediaProgramAdd.videoCodec">
            <td><select name="videoCodec">
                <c:forEach items="${mediaProgramAdd.videoCodecs}" var="vcodecOption">
                    <option value="${vcodecOption}" <c:if test="${mediaProgramAdd.videoCodec == vcodecOption}">selected="true"</c:if>>
                        <c:out value="${vcodecOption.name}" />
                    </option>
                </c:forEach>
            </select></td>
            <td><span class="error"><c:out value="${status.errorMessage}" /></span></td>
        </spring:bind>
    </tr>
    <tr class="videoStuff">
        <td align="right"><fmt:message key="medias.program.add.vb" /></td>
        <spring:bind path="mediaProgramAdd.videoBitrate">
            <td><input type="text" name="videoBitrate" value="<c:out value="${status.value}" />" /></td>
            <td><span class="error"><c:out value="${status.errorMessage}" /></span></td>
        </spring:bind>
    </tr>
    <tr class="videoStuff">
        <td align="right"><fmt:message key="medias.program.add.scale" /></td>
        <spring:bind path="mediaProgramAdd.scale">
            <td><input type="text" name="scale" value="<c:out value="${status.value}" />" /></td>
            <td><span class="error"><c:out value="${status.errorMessage}" /></span></td>
        </spring:bind>
    </tr>
    <tr class="videoStuff">
        <td align="right"><fmt:message key="medias.program.add.deinterlace" /></td>
        <spring:bind path="mediaProgramAdd.deinterlace">
            <td><input type="checkbox" name="deinterlace" /></td>
            <td><span class="error"><c:out value="${status.errorMessage}" /></span></td>
        </spring:bind>
    </tr>
    <tr>
        <td align="right"><fmt:message key="medias.program.add.transcodeaudio" /></td>
        <spring:bind path="mediaProgramAdd.transcodeAudio">
            <td><input id="transcodeAudio" type="checkbox" name="transcodeAudio" onchange="onTranscodeAudioUpdate(this.checked);" /></td>
            <td><span class="error"><c:out value="${status.errorMessage}" /></span></td>
        </spring:bind>
    </tr>
    <tr class="audioStuff">
        <td align="right"><fmt:message key="medias.program.add.acodec" /></td>
        <spring:bind path="mediaProgramAdd.audioCodec">
            <td><select name="audioCodec">
                <c:forEach items="${mediaProgramAdd.audioCodecs}" var="acodecOption">
                    <option value="${acodecOption}" <c:if test="${mediaProgramAdd.audioCodec == acodecOption}">selected="true"</c:if>>
                        <c:out value="${acodecOption.name}" />
                    </option>
                </c:forEach>
            </select></td>
            <td><span class="error"><c:out value="${status.errorMessage}" /></span></td>
        </spring:bind>
    </tr>
    <tr class="audioStuff">
        <td align="right"><fmt:message key="medias.program.add.ab" /></td>
        <spring:bind path="mediaProgramAdd.audioBitrate">
            <td><input type="text" name="audioBitrate" value="<c:out value="${status.value}" />" /></td>
            <td><span class="error"><c:out value="${status.errorMessage}" /></span></td>
        </spring:bind>
    </tr>
    <tr>
        <td align="right"><fmt:message key="medias.program.add.announcements" /></td>
        <td><c:forEach items="${mediaProgramAdd.announcementCheckboxes}" var="announcementOption" varStatus="loopStatus">
            <spring:bind path="mediaProgramAdd.announcementCheckboxes[${loopStatus.index}].checked">
                <input type="hidden" name="_<c:out value="${status.expression}"/>" />
                <input id="announcements_<c:out value="${announcementOption.announcement}"/>" type="checkbox" name="<c:out value="${status.expression}"/>" <c:if test="${announcementOption.checked}">checked="checked"</c:if> />
                <c:out value="${announcementOption.announcement.name}" />
                ${status.errorCode}
                <br />
            </spring:bind>
        </c:forEach></td>
    </tr>
</table>
<spring:hasBindErrors name="mediaProgramAdd">
    <b><fmt:message key="medias.program.add.error.fix" /></b>
</spring:hasBindErrors> <br />
<input type="submit" value="<fmt:message key="medias.program.add.run" />">
</form>

<div id="ipHelp">
<table><tr><td style="width: 30px">
<img src="<c:url value="/img/help.png" />" alt="Help" />
</td><td>
<fmt:message key="medias.program.add.ip.help" /><br />
<fmt:message key="medias.program.add.id.help" />
</td></tr></table>
</div>

<script type="text/javascript">

/**
 * See http://www.videolan.org/streaming-features.html and 
 * http://wiki.videolan.org/Documentation:Streaming_HowTo/Advanced_Streaming_Using_the_Command_Line#sap
 */

function isCompatible1(type, protocol) {
    if(type == "VOD") {
        return protocol == "RTSP";
    } else {
        return protocol != "RTSP";
    }
}

function isCompatible2(protocol, mux) {
    if(protocol == "UDP_MULTICAST") {
        return mux == "TS";
    } else if(protocol == "RTSP") {
        return mux == "RAW";
    }
    return true;
}

function updateProtocol() {
    var select = document.getElementById("type");
    var type = select.options[select.selectedIndex].value;
    var protocolSelect = document.getElementById("protocol");
    var options = protocolSelect.options;
    var currentOptionCompatible = isCompatible1(type, options[protocolSelect.selectedIndex].value);
    for(var i=0; i < options.length; i++) {
        var protocol = options[i].value;
        var compatible = isCompatible1(type, protocol);
        options[i].disabled = !compatible;
        if(!currentOptionCompatible && compatible) {
            protocolSelect.selectedIndex = i;
            currentOptionCompatible = true;
        }
    }
}

function updateMux() {
    var select = document.getElementById("protocol");
    var protocol = select.options[select.selectedIndex].value;
    if(protocol == "UDP_MULTICAST") {
        document.getElementById("announcements_SAP").disabled = false;
        document.getElementById("ipTr").style.display = "table-row";
        document.getElementById("ipHelp").style.display = "block";
    } else {
        document.getElementById("ipTr").style.display = "none";
        document.getElementById("ipHelp").style.display = "none";
        var input = document.getElementById("announcements_SAP");
        input.checked = false;
        input.disabled = true;
    }
    var muxSelect = document.getElementById("mux");
    var options = muxSelect.options;
    var currentOptionCompatible = isCompatible2(protocol, options[muxSelect.selectedIndex].value);
    for(var i=0; i < options.length; i++) {
        var mux = options[i].value;
        var compatible = isCompatible2(protocol, mux);
        options[i].disabled = !compatible;
        if(!currentOptionCompatible && compatible) {
            muxSelect.selectedIndex = i;
            currentOptionCompatible = true;
        }
    }
}

/**
 * Modification chain:
 *   Type -> Protocol -> Mux
 */

function onTypeUpdate() {
    updateProtocol();
    onProtocolUpdate();
}

function onProtocolUpdate() {
     updateMux();
     onMuxUpdate();
}

function onMuxUpdate() {
    // Nothing to to.
}

function onTranscodeVideoUpdate(checked) {
    videoStuff = document.getElementsByClassName("videoStuff");
    for (var i = 0; i < videoStuff.length; i++) {
        videoStuff[i].style.display = checked ? "table-row" : "none";
    }
}

function onTranscodeAudioUpdate(checked) {
    audioStuff = document.getElementsByClassName("audioStuff");
    for (var i = 0; i < audioStuff.length; i++) {
        audioStuff[i].style.display = checked ? "table-row" : "none";
    }
}

onTypeUpdate();
onTranscodeVideoUpdate(document.getElementById("transcodeVideo").checked);
onTranscodeAudioUpdate(document.getElementById("transcodeAudio").checked);

</script>