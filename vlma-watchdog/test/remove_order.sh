#! /bin/sh
order_id=$1

if [ "${order_id}" = "" ]; then
  echo "Usage: remove_order.sh <order_id>"
  exit 1
fi

curl -d "id=${order_id}" --user videolan:admin http://localhost:4213/order/remove

