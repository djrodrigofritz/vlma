#! /usr/bin/env python
# -*- coding: utf-8 -*-

# Copyright (C) 2009-2010 Adrien Grand
#
# This file is part of VLMa.
#
# VLMa is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 2 of the License, or
# (at your option) any later version.
#
# VLMa is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with VLMa. If not, see <http://www.gnu.org/licenses/>.


from api import *
import constants, os, utils, socket

if utils.platform_is_windows:
  import win32api

class VLCOption(constants.Enum):
  def __init__(self, name):
    constants.Enum.__init__(self, name)

VLCOption.TELNET_PORT = VLCOption("TELNET_PORT")

DEFAULT_VLC_OPTIONS = {

  VLCOption.TELNET_PORT: 4214

}


class VLC(Streamer):

  def __init__(self, sid, cmd, options = {}):
    opts = DEFAULT_VLC_OPTIONS.copy()
    for (k, v) in options.items():
      opts[k] = v
    Streamer.__init__(self, sid, cmd, opts)
    self.type = "VLC"
    version = self._get_version()
    if version.find(" 0.8.") >= 0:
      args = ["-vvv", "--rtsp-host", "0.0.0.0:5554", "--intf", "telnet", "--telnet-port", "%d" %self.options[VLCOption.TELNET_PORT]]
    else:
      args = ["-vvv", "-q", "--ignore-config", "--rtsp-host", "0.0.0.0:5554", "--intf", "lua", "--lua-intf", "telnet", "--lua-config", "telnet={hosts={'*:%d'}}" %self.options[VLCOption.TELNET_PORT]]
    self.start(args)

  def _get_version(self):
    version = "Unknown"
    if utils.platform_is_windows:
      key_name = "SOFTWARE\\VideoLAN\\VLC"
      try:
        reg_key = win32api.RegOpenKeyEx(win32con.HKEY_CURRENT_USER, key_name, 0, win32con.KEY_READ)
      except:
        try:
          reg_key = win32api.RegOpenKeyEx(win32con.HKEY_LOCAL_MACHINE, key_name, 0, win32con.KEY_READ)
        except:
          self._logger.warn("Could not find the version of VLC")
      version = win32api.RegQueryValueEx(reg_key, "Version")[0]
      reg_key.close()
      version = "VLC media player" + version
    else:
      try:
        out = os.popen2("%s --version" %self.cmd)[1]
        version = out.readline().rstrip("\n")
      except Exception, e:
        self._logger.warn(e)
    return version

  def can_stream(self, order):
    return True

  def _waitForPrompt(self, myS):
    PASSWORD_PROMPT = "Password: \xff\xfb\x01"
    CMD_PROMPT = "> "
    test1 = ""
    test2 = ""
    while(test1 != CMD_PROMPT and test2 != PASSWORD_PROMPT ):
      buf = myS.recv(256)
      test1 = buf[-len(CMD_PROMPT):]
      test2 = buf[-len(PASSWORD_PROMPT):]

  def _start_announcing(self, order):
    pass

  def _stop_announcing(self, order):
    pass

  def _sendCmd(self, myS, orderStr):
    myS.send(orderStr + "\n")
    self._waitForPrompt(myS)

  def _destToSoutStr(self, dest):
    if dest.streaming.protocol == "udp":
      ret = "dst=standard{mux=%s,access=%s,dst=%s:%d" \
          % (dest.streaming.mux, dest.streaming.protocol, dest.ip, dest.port)
    elif dest.streaming.protocol == "rtp":
      ret = "dst=rtp{mux=%s,dst=%s,port=%d" \
          % (dest.streaming.mux, dest.ip, dest.port)
    if not dest.announcing is None and dest.announcing.type == "sap":
      ret += ",sap,name=\"%s\"" % (dest.announcing.name)
      if not dest.announcing.group is None:
        ret += ",group=\"%s\"" % (dest.announcing.group)
    ret += "}"
    return ret

  def _connectToVLC(self):
    # Connect to the VLC telnet interface
    myS = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    myS.connect(("localhost", self.options[VLCOption.TELNET_PORT]))
    # Wait for the password invitation
    self._waitForPrompt(myS)
    # Give the password
    self._sendCmd(myS, "admin")
    return myS

  def _removeOrder(self, order, myS):
    self._sendCmd(myS, "setup %s disabled" % order.id)
    self._sendCmd(myS, "control %s stop" % order.id)
    self._sendCmd(myS, "del %s" % order.id)

  def start_streaming(self, order):
    # Connect to VLC
    myS = self._connectToVLC()
    # Stop the previous order with the same nane if one exists.
    self._removeOrder(order, myS)

    # Handle DVB orders.
    if isinstance(order, DVBOrder):
      # TODO handle schedule and VoD ?
      self._sendCmd(myS, "new %s broadcast enabled\n" % order.id)
      self._sendCmd(myS, "setup %s input \"dvb://\"" % order.id)
      self._sendCmd(myS, "setup %s option dvb-adapter=%d" % (order.id, order.adapter))
      self._sendCmd(myS, "setup %s option dvb-frequency=%d" % (order.id, order.frequency))
      # Give PIDs
      cmd = "setup %s option programs=" % order.id
      for src in order.programs.keys():
        cmd += "%s," % str(src)
      # Remove the last comma
      cmd = cmd[:-1]
      self._sendCmd(myS, cmd)
      # Special case of DVB-S.
      if isinstance(order, DVBSOrder):
        self._sendCmd(myS, "setup %s option dvb-voltage=%d" % (order.id, order.voltage))
        self._sendCmd(myS, "setup %s option dvb-srate=%d" % (order.id, order.srate))
        self._sendCmd(myS, "setup %s option dvb-fec=%d" % (order.id, order.fec))
        self._sendCmd(myS, "setup %s option dvb-bandwidth=%d" % (order.id, order.bandwidth))
      # Prepare sout command line.
      cmd = "setup %s output #duplicate{" % order.id
      for src, dests in order.programs.items():
        # TODO Handle SAP announces, handle multiple destinations.
        cmd += self._destToSoutStr(dests[0]) + ",select=\"program=%s\"," % str(src)
      # Remove the last comma, close the duplicate structure.
      cmd = cmd[:-1] + "}"
      self._sendCmd(myS, cmd)

    # Start the new order.
    self._sendCmd(myS, "control %s play" % order.id)
    # Close the telnet connection
    myS.send("quit\n")

  def stop_streaming(self, order):
    # Connect to VLC
    myS = self._connectToVLC()
    # Stop and remove the order
    self._removeOrder(order, myS)
    # Close the telnet connection
    myS.send("quit\n")

