#! /usr/bin/env python
# -*- coding: utf-8 -*-

# Copyright (C) 2009-2010 Adrien Grand
#
# This file is part of VLMa.
#
# VLMa is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 2 of the License, or
# (at your option) any later version.
#
# VLMa is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with VLMa. If not, see <http://www.gnu.org/licenses/>.


import conf, constants, os.path, sap, tempfile
from api import *

class DVBlastOption(constants.Enum):
  def __init__(self, name):
    constants.Enum.__init__(self, name)

DVBlastOption.ADAPTER = DVBlastOption("ADAPTER")

DEFAULT_DVBLAST_OPTIONS = {

  DVBlastOption.ADAPTER: 0

}

class DVBlast(Streamer):
  """A DVBlast streamer."""

  def __init__(self, sid, cmd, options={}):
    opts = DEFAULT_DVBLAST_OPTIONS.copy()
    for k, v in options.items():
      opts[k] = v
    Streamer.__init__(self, sid, cmd, opts)
    self.type = "DVBLAST"
    self.filename = os.path.join(os.path.abspath(os.path.curdir), "dvblast-" + self.id + ".conf")
    self.__sap_server = sap.SAPServer(conf.NETWORK_ADDRESS)
    self.__sap_server.start()

  def can_stream(self, order):
    # DVBlast can stream if the order is a DVB one and if there is no
    # transcoding required
    if isinstance(order, DVBOrder):
      if order.adapter != self.options[DVBlastOption.ADAPTER]:
        return False
      for src, dests in order.programs.items():
        for dest in dests:
          if not dest.transcoding is None or not dest.streaming.type == "broadcast" or not dest.streaming.protocol in ["rtp", "udp"] or not dest.streaming.mux in ["ts", "raw"]:
            break
      return True
    return False

  def _write_conf_file(self, order):
    conf_file = []
    for src, dests in order.programs.items():
      for dest in dests:
        d = dest.ip + ":" + str(dest.port)
        if dest.streaming.protocol == "udp":
          d += "/udp"
        conf_file += [d, " ", "1", " ", str(src), "\n"]
    f = open(self.filename, 'w')
    f.writelines(conf_file)
    f.close()

  def _start_announcing(self, order):
    for dests in order.programs.values():
      for dest in dests:
        if not dest.announcing is None and dest.announcing.type == "sap":
          self.__sap_server.add_dest(dest)

  def _stop_announcing(self, order):
    self.__sap_server.remove_dests()

  def start_streaming(self, order):
    args = ["-e",
            "-f", str(order.frequency),
            "-a", str(order.adapter),
            "-c", self.filename,
            "-t", str(order.ttl),
            "-i", "1"]
    if isinstance(order, DVBSOrder):
      args += ["-s", str(order.srate),
               "-v", str(order.voltage),
               "-b", str(order.bandwidth)]
    self._write_conf_file(order)
    self._start_announcing(order)
    self.start(args)

  def stop_streaming(self, order):
    self._stop_announcing(order)
    self.stop()

