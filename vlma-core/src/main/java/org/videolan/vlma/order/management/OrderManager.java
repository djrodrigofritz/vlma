/*
 * Copyright (C) 2008-2009 the VideoLAN team
 *
 * This file is part of VLMa.
 *
 * VLMa is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * VLMa is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with VLMa. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package org.videolan.vlma.order.management;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;
import org.videolan.vlma.model.Adapter;
import org.videolan.vlma.model.Order;
import org.videolan.vlma.model.Program;
import org.videolan.vlma.model.Server;
import org.videolan.vlma.monitor.Monitor;
import org.videolan.vlma.order.management.OrderState.State;
import org.videolan.vlma.order.sender.OrderSender;
import org.videolan.vlma.order.sender.TelnetConnection;
import org.videolan.vlma.order.watcher.StreamWatcher;

/**
 * Order manager. This class provides methods for managing the orders lifecycle.
 *
 * @author Adrien Grand
 */
public class OrderManager {

    private static final Logger logger = Logger.getLogger(OrderManager.class);

    private OrderComputer orderComputer;
    private OrderSender orderSender;
    private StreamWatcher streamWatcher;
    private final OrderRegistry orders;

    public OrderManager() {
        orders = new OrderRegistry();
    }

    /**
     * Update the state of medias.
     *
     * @return true upon changes
     */
    public synchronized boolean updateMedias() {
        boolean result = false;

        for(OrderState entry : orders.entries()) {
            State state = entry.getState();

            if (state.equals(State.STOPPED) || state.equals(State.TO_STOP))
                continue;

            boolean broadcasted = true;
            for(Program program : entry.getOrder().getPrograms()) {
                if (program.isTimeToPlay()) {
                    boolean broadcastState = program.getBroadcastState();
                    if (!streamWatcher.isPlayed(program)) {
                        logger.info("Program " + program.getName() + " is currently not played.");
                        program.setBroadcastState(false);
                        result = true;
                        broadcasted = false;
                    } else {
                        program.setBroadcastState(true);
                    }
                    if (broadcastState != program.getBroadcastState()) {
                        Monitor.dispatch(program.getBroadcastState() ?
                                Monitor.MediaEvent.BROADCASTED :
                                    Monitor.MediaEvent.NOT_BROADCASTED, program);
                    }
                }
            }
            entry.setState(broadcasted ? State.STARTED : State.TO_START);

            // Update the adapter's score
            Adapter adapter = entry.getOrder().getAdapter();
            if (broadcasted) {
                adapter.setScore(adapter.getScore() + 5);
            } else {
                adapter.setScore(adapter.getScore() - 1);
            }

        }
        return result;
    }

    /**
     * Update the list of orders.
     *
     * @return true upon change
     */
    public synchronized boolean updateOrders() {
        boolean result = updateMedias();

        long start = System.currentTimeMillis();
        Set<Order> computedOrders = orderComputer.computeOrders();
        if (logger.isDebugEnabled()) {
            logger.debug(Integer.toString(computedOrders.size()) + " orders computed in " + (System.currentTimeMillis() - start) + "ms");
        }

        Map<String, Order> newOrders = new HashMap<String, Order>();
        for(Order order : computedOrders) {
            newOrders.put(order.getId(), order);
        }

        // 1. Update the state of orders
        for (OrderState entry : orders.entries()) {
            Order order = newOrders.get(entry.getOrder().getId());
            if(order == null) {
                if(!entry.getState().equals(State.STOPPED)) {
                    // The order is not among new orders and is not stopped
                    // => We have to stop this order
                    result = true;
                    entry.setState(State.TO_STOP);
                }
            } else if(!order.equals(entry.getOrder())) {
                // This order has been updated
                // => Update the order and start it
                result = true;
                // We are losing the reference to the former order now, so we
                // need to update some fields here
                entry.getOrder().getAdapter().setBusy(false);
                // Set the new order
                entry.setOrder(order);
                entry.setState(State.TO_START);
            } else if(!entry.getState().equals(State.STARTED)) {
                // This order has either been stopped or is not started yet
                result = true;
                entry.setState(State.TO_START);
            }
        }
        // 2. Add missing orders
        for (Order order : newOrders.values()) {
            if (!orders.contains(order.getId())) {
                orders.add(order);
                result = true;
            }
        }
        // 3. Update programs
        for (OrderState entry : orders.entries()) {
            Order order = entry.getOrder();
            switch (entry.getState()) {
            case TO_STOP:
                // Nothing to do
                break;
            case STOPPED:
                order.getAdapter().setBusy(false);
                for (Program program : order.getPrograms()) {
                    program.setAdapter(null);
                }
                break;
            case TO_START:
            case STARTED:
                order.getAdapter().setBusy(true);
                for (Program program : order.getPrograms()) {
                    program.setAdapter(order.getAdapter());
                }
                break;
            default:
                assert false : "Unknown state";
            }
        }

        return result;
    }

    /**
     * Give orders.
     */
    public void giveOrders() {
        Map<Server, SendOrder> sendThreads = new HashMap<Server, SendOrder>();
        synchronized (this) {
            for (OrderState entry : orders.entries()) {
                Order order = entry.getOrder();
                SendOrder sendThread = sendThreads.get(order.getAdapter().getServer());
                if (sendThread == null) {
                    sendThread = new SendOrder();
                    sendThreads.put(order.getAdapter().getServer(), sendThread);
                }
                switch(entry.getState()) {
                case TO_START:
                    sendThread.addOrder(order);
                    break;
                case TO_STOP:
                    // Don't try to stop the order if the server is down
                    if (order.getAdapter().getServer().isUp()) {
                        sendThread.removeOrder(order);
                    }
                    break;
                default:
                    break;
                }
            }
        }
        // Start threads, one per server
        for (Thread thread : sendThreads.values()) {
            thread.start();
        }
        // Wait for all threads to finish
        for (Thread thread : sendThreads.values()) {
            try {
                thread.join();
            } catch (InterruptedException e) { }
        }
    }

    private class SendOrder extends Thread {

        private Server server;
        private List<Order> ordersToStart;
        private List<Order> ordersToStop;

        public SendOrder() {
            server = null;
            ordersToStart = new ArrayList<Order>();
            ordersToStop = new ArrayList<Order>();
        }

        public void addOrder(Order order) {
            this.ordersToStart.add(order);
            if (server == null)
                server = order.getAdapter().getServer();
        }

        public void removeOrder(Order order) {
            this.ordersToStop.add(order);
            if (server == null)
                server = order.getAdapter().getServer();
        }

        public void run() {
            // Can happen if one server which is not up has orders to stop
            if (server == null) return;
            TelnetConnection conn = null;
            logger.debug("Sending orders to " + server.getName());
            try {
                conn = orderSender.getConn(server);
                for(Order order : ordersToStop) {
                    orderSender.stop(conn, order);
                    synchronized(OrderManager.this) {
                        orders.get(order.getId()).setState(OrderState.State.STOPPED);
                    }
                }
                if (logger.isDebugEnabled()) {
                    logger.debug("Sending " + ordersToStart.size() + " orders to " + server.getName());
                }
                for(Order order : ordersToStart) {
                    orderSender.start(conn, order);
                }
            } catch (IOException e) {
                logger.error("Error while trying to send an order to " + server.getName(), e);
            } finally {
                if (conn != null) {
                    try {
                        logger.debug("Closing socket to " + server.getName());
                        conn.close();
                        logger.debug("Socket closed (" + server.getName() + ")");
                    } catch (IOException e1) {
                        logger.debug("Error while closing connection", e1);
                    }
                }
            }
        }
    }

    /**
     * @param orderComputer the orderComputer to set
     */
    public void setOrderComputer(OrderComputer orderComputer) {
        this.orderComputer = orderComputer;
    }

    /**
     * @param orderSender the orderSender to set
     */
    public void setOrderSender(OrderSender orderSender) {
        this.orderSender = orderSender;
    }

    /**
     * @param streamWatcher the streamWatcher to set
     */
    public void setStreamWatcher(StreamWatcher streamWatcher) {
        this.streamWatcher = streamWatcher;
    }

}
