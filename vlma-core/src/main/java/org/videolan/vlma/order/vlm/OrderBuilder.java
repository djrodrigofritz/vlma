/*
 * Copyright (C) 2008-2009 the VideoLAN team
 *
 * This file is part of VLMa.
 *
 * VLMa is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * VLMa is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with VLMa. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package org.videolan.vlma.order.vlm;

import java.util.HashSet;
import java.util.Set;

import org.apache.commons.configuration.Configuration;
import org.videolan.vlma.model.DTTProgramGroup;
import org.videolan.vlma.model.DVBMedia;
import org.videolan.vlma.model.DVBProgramGroup;
import org.videolan.vlma.model.FilesChannel;
import org.videolan.vlma.model.FilesProgramGroup;
import org.videolan.vlma.model.Order;
import org.videolan.vlma.model.Program;
import org.videolan.vlma.model.ProgramGroup;
import org.videolan.vlma.model.SatProgramGroup;
import org.videolan.vlma.model.StreamProgramGroup;
import org.videolan.vlma.model.TranscodingStrategy;
import org.videolan.vlma.model.AnnouncingStrategy.Announcement;

/**
 * Transforms an Order object to something that looks like what the
 * representation of a VLM media.
 * See <b>vlm_media_t</b> in <a href="http://git.videolan.org/?p=vlc.git;a=blob;f=include/vlc_vlm.h">include/vlc_vlm.h</a>
 *
 * @author Adrien Grand
 */
public class OrderBuilder {

    private Configuration configuration;

    public VLMOrder buildOrder(Order order) {
        if (order.getPrograms().isEmpty()) {
            throw new IllegalArgumentException("Cannot build a VLM order for an empty group");
        }
        switch (order.getPrograms().getType()) {
        case BROADCAST:
            return buildBroadcast(order);
        case VOD:
            return buildVod(order);
        default:
            throw new IllegalArgumentException("Unknown StreamingStrategy type");
        }
    }

    private VLMBroadcast buildBroadcast(Order order) {
        VLMBroadcast result = new VLMBroadcast();
        result.setName(order.getName());
        ProgramGroup group = order.getPrograms();
        // FilesChannel must loop
        if (group instanceof FilesProgramGroup) {
            result.setLoop(true);
            result.addOption("sout-keep");
        }
        // Set input
        if (group instanceof FilesProgramGroup) {
            FilesProgramGroup g = (FilesProgramGroup)group;
            result.setInputs(g.getMedia().getFiles());
        } else if (group instanceof StreamProgramGroup) {
            assert group.size() == 1;
            StreamProgramGroup g = (StreamProgramGroup)group;
            result.addInput(g.getMedia().getStreamURL());
        } else {
            result.addInput(VLMOrder.DVB_INPUT);
            result.addOption("dvb-adapter=" + order.getAdapter().getName());
            if (group instanceof DTTProgramGroup) {
                DTTProgramGroup g = (DTTProgramGroup)group;
                result.addOption("dvb-bandwidth=" + configuration.getInt("vlc.stream.dvb-bandwidth"));
                result.addOption("dvb-frequency=" + g.getFrequency());
            } else if (group instanceof SatProgramGroup) {
                SatProgramGroup g = (SatProgramGroup)group;
                result.addOption("dvb-frequency=" + g.getFrequency());
                switch (g.getPolarisation()) {
                case 'V':
                case 'v':
                    result.addOption("dvb-voltage=13");
                    break;
                case 'H':
                case 'h':
                    result.addOption("dvb-voltage=18");
                    break;
                }
                result.addOption("dvb-srate=" + g.getSymbolRate());
                result.addOption("dvb-fec=" + g.getErrorCorrection());
            }
            Set<Integer> programs = new HashSet<Integer>();
            for (Program program : group) {
                DVBMedia media = (DVBMedia) program.getMedia();
                programs.add(media.getSid());
            }
            StringBuilder builder = new StringBuilder("programs=");
            for (Integer id : programs) {
                builder.append(id).append(',');
            }
            result.addOption(builder.substring(0, builder.length() - 1));
        }
        result.addOption("ttl=" + configuration.getInt("vlc.stream.ttl"));
        // Set stream output
        result.setSout(buildSout(order.getPrograms()));
        return result;
    }

    private VLMVod buildVod(Order order) {
        VLMVod result = new VLMVod();
        result.setName(order.getName());
        ProgramGroup group = order.getPrograms();
        if (group instanceof FilesProgramGroup) {
            assert group.size() == 1;
            FilesChannel ch = (FilesChannel) ((FilesProgramGroup) group).getMedia();
            // Only one media per VoD
            assert ch.getFiles().size() == 1 : "Only one input allowed for VoD";
            result.addInput(ch.getFiles().get(0));
        } else {
            assert false : "VoD is only allowed for FilesChannels: " + group.getClass();
        }
        return result;
    }

    private String buildSout(ProgramGroup group) {
        StringBuilder sout = new StringBuilder();
        if (!group.isEmpty() || group instanceof DVBProgramGroup) {
            sout.append("#duplicate{");
            for (Program program : group) {
                sout.append("dst=");
                buildDst(sout, program);
                sout.append(',');
            }
            sout.setCharAt(sout.length() - 1, '}');
        } else {
            sout.append('#');
            buildDst(sout, group.iterator().next());
        }
        return sout.toString();
    }

    private void buildTranscode(StringBuilder sout, Program program) {
        TranscodingStrategy ts = program.getTranscodingStrategy();
        sout.append("transcode{");
        if (ts.getVideoCodec() != null) {
            sout.append("vcodec=").append(ts.getVideoCodec().getShortName()).append(",");
            sout.append("vb=").append(ts.getVideoBitrate()).append(",");
            sout.append("scale=").append(ts.getScale()).append(",");
        }
        if (ts.getAudioCodec() != null) {
            sout.append("acodec=").append(ts.getAudioCodec().getShortName()).append(",");
            sout.append("ab=").append(ts.getAudioBitrate()).append(",");
        }
        if (ts.getVideoCodec() != null && ts.isDeinterlace()) {
            sout.append("deinterlace,");
        }
        sout.setCharAt(sout.length() - 1, '}');
    }

    private void buildDst(StringBuilder dst, Program program) {
        if (program.getTranscodingStrategy() != null) {
            dst.append('\'');
            buildTranscode(dst, program);
            dst.append(':');
        }
        String destination = null;
        switch (program.getStreamingStrategy().getProtocol()) {
        case UDP_MULTICAST:
            destination = program.getIp().getHostAddress();
            break;
        case HTTP:
            destination = String.format("%s:%d/%s", program.getPlayer().getAddress(),
                    configuration.getInt("vlma.streaming.http.port"),
                    program.getId());
            break;
        default:
            assert false : "Unexpected protocol: " + program.getStreamingStrategy().getProtocol();
        }
        dst.append(String.format("standard{mux=%s,access=%s,dst=%s",
                program.getStreamingStrategy().getEncapsulation().getShortName(),
                program.getStreamingStrategy().getProtocol().getScheme(),
                destination));
        if (program.getAnnouncingStrategy().isEnabled(Announcement.SAP)) {
            dst.append(",sap,name=\"").append(program.getName()).append('\"');
            if (program.getGroup() != null && program.getGroup().length() > 0) {
                dst.append(",group=\"").append(program.getGroup()).append('\"');
            }
        }
        dst.append('}');
        if (program.getMedia() instanceof DVBMedia) {
            dst.append(",select=\"program=").append(((DVBMedia)program.getMedia()).getSid()).append("\"");
        }
        if (program.getTranscodingStrategy() != null) {
            dst.append('\'');
        }
    }

    /**
     * @param configuration the configuration to set
     */
    public void setConfiguration(Configuration configuration) {
        this.configuration = configuration;
    }

}
