/*
 * Copyright (C) 2006-2008 the VideoLAN team
 *
 * This file is part of VLMa.
 *
 * VLMa is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * VLMa is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with VLMa. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package org.videolan.vlma.monitor;

import org.apache.commons.configuration.Configuration;
import org.apache.log4j.Logger;
import org.videolan.vlma.model.Program;
import org.videolan.vlma.model.Server;
import org.videolan.vlma.notifier.Notifier;
import org.videolan.vlma.order.management.OrderManager;

/**
 * This class is the order monitoring thread.
 *
 * @author Sylvain Cadilhac <sylv at videolan.org>
 * @author Adrien Grand <jpountz at videolan.org>
 */
public class OrderMonitor extends Monitor {

    private static final Logger logger = Logger.getLogger(OrderMonitor.class);

    private Configuration configuration;
    private OrderManager orderManager;
    private volatile boolean shouldRun;
    private boolean orderGivingPending;
    private volatile long lastOrderGiving;
    private Thread orderMonitor;
    private Runnable orderMonitorRunnable;

    public OrderMonitor() {
        orderMonitor = null;
        orderGivingPending = false;
        lastOrderGiving = 0L;
        registerMonitor(this);
        orderMonitorRunnable = new Runnable() {
            public void run() {
                while(shouldRun) {
                    try {
                        Thread.sleep(configuration.getLong("vlma.monitor.order.check.interval") * 1000L);
                    } catch (InterruptedException e) {
                        continue;
                    }
                    long currentTime = System.currentTimeMillis();
                    if (currentTime - lastOrderGiving > configuration.getLong("vlma.monitor.order.give.interval") * 1000L) {
                        lastOrderGiving = currentTime;
                        if (orderManager.updateOrders())
                            orderManager.giveOrders();
                        lastOrderGiving = currentTime;
                    } else {
                        orderManager.updateMedias();
                    }
                }
                logger.info("Order monitor stopped");
            }
        };
    }

    public synchronized boolean isRunning() {
        return (orderMonitor != null)
                && (orderMonitor.isAlive());
    }

    public synchronized void start() {
        if (isRunning()) {
            logger.warn("Order Monitor already running, stop it first");
            return;
        }
        logger.info("Starting order monitor");
        shouldRun = true;
        orderMonitor = new Thread(MonitorThreadGroup.getInstance(), orderMonitorRunnable);
        orderMonitor.setName("Order Monitor");
        orderMonitor.start();
    }

    public synchronized void stop() {
        logger.info("Stopping order monitor");
        shouldRun = false;
        orderMonitor.interrupt();
    }

    /**
     * @param configuration the configuration to set
     */
    public void setConfiguration(Configuration configuration) {
        this.configuration = configuration;
    }

    /**
     * @param orderManager the orderManager to set
     */
    public void setOrderManager(OrderManager orderManager) {
        this.orderManager = orderManager;
    }

    @Override
    protected void onServerDown(Server server) {
        scheduleOrderGiving();
    }

    @Override
    protected void onServerUp(Server server) {
        scheduleOrderGiving();
    }

    private void scheduleOrderGiving() {
        // No need to protect the access to orderGivingPending since
        // there cannot be concurrent access to the on* methods
        if(!orderGivingPending) {
            orderGivingPending = true;
            Thread orderGiver = new Thread() {
                public void run() {
                    try {
                        Thread.sleep(10000);
                    } catch (InterruptedException e) {
                        logger.error(e);
                    }
                    orderManager.updateOrders();
                    orderManager.giveOrders();
                    orderGivingPending = false;
                    lastOrderGiving = System.currentTimeMillis();
                }
            };
            orderGiver.setName("Order Giver");
            orderGiver.start();
        }
    }

    @Override
    protected void onProgramNotBroadcasted(Program program) {
        Notifier.dispatchNotification("[WARNING] Program " + program.getId() + " is *not* broadcasted");
    }

    @Override
    protected void onProgramBroadcasted(Program program) {
        Notifier.dispatchNotification("[INFO] Program " + program.getName() + " is now broadcasted");
    }

}
