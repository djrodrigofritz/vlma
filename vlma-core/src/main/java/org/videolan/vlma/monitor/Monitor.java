/*
 * Copyright (C) 2006-2008 the VideoLAN team
 *
 * This file is part of VLMa.
 *
 * VLMa is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * VLMa is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with VLMa. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package org.videolan.vlma.monitor;

import java.util.ArrayList;
import java.util.List;

import org.videolan.vlma.model.Program;
import org.videolan.vlma.model.Server;

/**
 * Parent of every monitor.
 *
 * @author Adrien Grand <jpountz at videolan.org>
 */
public abstract class Monitor {

    private static final List<Monitor> monitors = new ArrayList<Monitor>();
    private static final Object mutex = new Object();

    protected static void registerMonitor(Monitor monitor) {
        synchronized (mutex) {
            monitors.add(monitor);
        }
    }

    /**
     * Starts the monitor.
     */
    public abstract void start();

    /**
     * Stops the monitor.
     */
    public abstract void stop();

    public static enum ServerEvent {
        UP, DOWN, LOAD_TOO_HIGH, CPU_TOO_HIGH, MEM_TO_HIGH
    }

    public static enum MediaEvent {
        BROADCASTED, NOT_BROADCASTED
    }

    protected void onServerUp(Server server) {}
    protected void onServerDown(Server server) {}
    protected void onLoadToHigh(Server server, double value) {}
    protected void onCpuTooHigh(Server server, double value) {}
    protected void onMemTooHigh(Server server, double value) {}
    protected void onProgramBroadcasted(Program program) {}
    protected void onProgramNotBroadcasted(Program program) {}

    public static void dispatch(ServerEvent e, Server server) {
        synchronized (mutex) {
            for(Monitor monitor : monitors) {
                switch(e) {
                case UP:
                    monitor.onServerUp(server);
                    break;
                case DOWN:
                    monitor.onServerDown(server);
                    break;
                }
            }
        }
    }

    public static void dispatch(ServerEvent e, Server server, double value) {
        synchronized (mutex) {
            for(Monitor monitor : monitors) {
                switch(e) {
                case LOAD_TOO_HIGH:
                    monitor.onLoadToHigh(server, value);
                    break;
                case CPU_TOO_HIGH:
                    monitor.onCpuTooHigh(server, value);
                    break;
                case MEM_TO_HIGH:
                    monitor.onMemTooHigh(server, value);
                    break;
                }
            }
        }
    }

    public static void dispatch(MediaEvent e, Program program) {
        synchronized (mutex) {
            for(Monitor monitor : monitors) {
                switch(e) {
                case BROADCASTED:
                    monitor.onProgramBroadcasted(program);
                    break;
                case NOT_BROADCASTED:
                    monitor.onProgramNotBroadcasted(program);
                    break;
                }
            }
        }
    }

}
