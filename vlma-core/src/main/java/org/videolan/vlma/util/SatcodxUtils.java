/*
 * Copyright (C) 2006-2009 the VideoLAN team
 *
 * This file is part of VLMa.
 *
 * VLMa is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * VLMa is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with VLMa. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package org.videolan.vlma.util;

import java.io.IOException;
import java.net.URL;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.configuration.Configuration;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.apache.oro.text.PatternCacheRandom;
import org.apache.oro.text.perl.Perl5Util;
import org.htmlparser.Node;
import org.htmlparser.NodeFilter;
import org.htmlparser.Parser;
import org.htmlparser.filters.HasAttributeFilter;
import org.htmlparser.filters.OrFilter;
import org.htmlparser.filters.TagNameFilter;
import org.htmlparser.nodes.TextNode;
import org.htmlparser.util.NodeList;
import org.htmlparser.util.ParserException;
import org.videolan.vlma.model.Media;
import org.videolan.vlma.model.SatChannel;

/**
 * Utility class that provides functions to parse satellite channel directories
 * hosted at http://www.satcodx.com.
 *
 * @author Adrien Grand
 */
public class SatcodxUtils {

    /**
     * No instantiation allowed.
     */
    private SatcodxUtils() {}

    private static final Logger logger = Logger.getLogger(SatcodxUtils.class);

    private static final Perl5Util PERL5_UTIL = new Perl5Util(new PatternCacheRandom(10));

    private static final NodeFilter TH_TAG_FILTER = new OrFilter(new TagNameFilter("th"), new HasAttributeFilter("rowspan"));
    private static final NodeFilter TD_TAG_FILTER = new TagNameFilter("td");

    private static final String NAME         = "name";
    private static final String CATEGORY     = "category";
    private static final String COUNTRY      = "country";
    private static final String ENCRYPTION   = "encryption";
    private static final String FREQUENCY    = "frequency";
    private static final String SID          = "sid";
    private static final String COVERAGE     = "coverage";
    private static final String POLARIZATION = "polarization";
    private static final String SYMBOL_RATE  = "symbol-rate";
    private static final String FEC          = "fec";

    private static String[] fields = new String[] {
        NAME,
        FREQUENCY,
        SYMBOL_RATE,
        COUNTRY,
        ENCRYPTION,
        SID,
        COVERAGE,
        POLARIZATION,
        FEC,
        CATEGORY
    };

    /**
     * Extracts a single cell from a 2 dimensional table whose lines are
     * separated using the <tt>&lt;br /&gt;</tt> HTML element.
     *
     * @param nodes the row where each element contains a <tt><br /></tt>
     *              separated list of data
     * @param column the column to parse (index of <code>nodes</code>)
     * @param row the row we want to extract from the cell
     */
    private static String htmlCellExtract(NodeList nodes, int column, int row) {
        String result = null;
        NodeFilter brTagFilter = new TagNameFilter("br");
        NodeList breakLines = nodes.elementAt(column).getChildren().extractAllNodesThatMatch(brTagFilter);

        int nbLines = breakLines.size();
        if (nbLines == 0) {
            Node text = nodes.elementAt(column);
            result = text.toPlainTextString();
        } else {
            Node text;
            if (row == 0) {
                text = breakLines.elementAt(0).getPreviousSibling();
                while (!(text instanceof TextNode) ||
                        StringUtils.isBlank(((TextNode) text).getText())) {
                    text = text.getPreviousSibling();
                }
            } else {
                text = breakLines.elementAt(row - 1).getNextSibling();
                while (!(text instanceof TextNode) ||
                        StringUtils.isBlank(((TextNode) text).getText())) {
                    text = text.getNextSibling();
                }
            }
            result = ((TextNode) text).getText();
        }

        result = result.replaceAll("&nbsp;", " ");
        result = result.trim();
        return result;
    }

    /**
     * Retrieve a list of satellite channels from a {@link URL}.
     *
     * @param source the {@link URL} to analyze
     * @param configuration configuration containing description from the
     *                      layout of the source
     * @param newMedias the collection to fill
     * @throws IOException
     */
    @SuppressWarnings("unchecked")
    public static void getSatChannels(URL source, Configuration satcodxConfiguration,
            Collection<Media> newMedias) throws IOException {

        String url = source.toString();
        logger.info("Downloading satellite channels descriptions from " + url);
        NodeList rows = null;
        Parser parser;
        try {
            parser = new Parser(source.openConnection());
            rows = parser.parse(new TagNameFilter("tr"));
        } catch (ParserException e) {
            // don't throw new IOException(e) because it would require
            // ParserException to be in the classpath of the client
            throw new IOException(e.getMessage());
        }
        logger.info(url + " downloaded and parsed");

        List<String> categoryFilter = satcodxConfiguration.getList("category-filter");

        // Default values, defined only for a few fields
        final Map<String, String> defaults = new HashMap<String, String>();

        // Common field values which are common for a group of channels (the
        // frequency, typically)
        final Map<String, String> channelCommonFields = new HashMap<String, String>();

        // Fields which are specific to the channel
        final Map<String, String> channel = new HashMap<String, String>();

        // Fill the default map, it won't be accessed for writing later in the
        // method
        for (String field : fields) {
            String key = field + ".default";
            String value;
            if ((value = satcodxConfiguration.getString(key)) != null) {
                defaults.put(field, value);
            }
        }

        int nbThCols = satcodxConfiguration.getInt("th.nbcols");
        int nbTdCols = satcodxConfiguration.getInt("td.nbcols");
        final Map<String, NodeList> nodeLists = new HashMap<String, NodeList>();

        for (int i = 0; i < rows.size(); ++i) {
            Node node = rows.elementAt(i);
            NodeList thCells = node.getChildren().extractAllNodesThatMatch(TH_TAG_FILTER);
            NodeList tdCells = node.getChildren().extractAllNodesThatMatch(TD_TAG_FILTER);
            nodeLists.put("th", thCells);
            nodeLists.put("td", tdCells);
            if (tdCells.size() == nbTdCols) { // new channel to parse
                channel.clear();
                channel.putAll(defaults);

                boolean skip = false;
                for (String field : fields) {
                    String owner = satcodxConfiguration.getString(field + ".owner");
                    if ("td".equals(owner) || (("th").equals(owner) && thCells.size() == nbThCols)) {

                        NodeList nodes = nodeLists.get(owner);
                        if (nodes == null)
                            throw new IllegalStateException("Wrong value for " + field +
                                    ".owner, only th and tr are valid options");
                        int column = satcodxConfiguration.getInt(field + ".column");
                        int row = satcodxConfiguration.getInt(field + ".row");
                        String value = htmlCellExtract(nodes, column, row);

                        String pattern = satcodxConfiguration.getString(field + ".match");
                        synchronized (PERL5_UTIL) {
                            if (pattern != null && !PERL5_UTIL.match(pattern, value)) {
                                logger.warn("Value: \"" + value + "\" for field " + field +
                                        " didn't match \"" + pattern + "\"");
                                if (!defaults.containsKey(field)) {
                                    logger.warn("Could not extract value for the field " + field +
                                            "for media " + channel.get("name"));
                                    skip = true;
                                    break;
                                } else {
                                    continue;
                                }
                            }

                            List<String> extractions = satcodxConfiguration.getList(field + ".extractions");
                            for (String extraction : extractions) {
                                if (PERL5_UTIL.match(extraction, value)) {
                                    value = PERL5_UTIL.group(1);
                                }
                            }

                            List<String> substitutions = satcodxConfiguration.getList(field + ".substitutions");
                            for (String substitution : substitutions) {
                                value = PERL5_UTIL.substitute(substitution, value);
                            }

                            if (field.equals(CATEGORY) && !categoryFilter.contains(value)) {
                                skip = true;
                                logger.info("Skipping " + channel.get("name") +
                                        " because category is " + value);
                                break;
                            }
                        }

                        // Where to write data
                        if ("td".equals(owner)) {
                            channel.put(field, value);
                        } else {
                            channelCommonFields.put(field, value);
                        }
                    }
                }
                if (skip) continue;

                channel.putAll(channelCommonFields);

                SatChannel satChannel = new SatChannel();
                satChannel.setName(channel.get(NAME));
                satChannel.setCoverage(channel.get(COVERAGE));
                satChannel.setFrequency(Integer.parseInt(channel.get(FREQUENCY)));
                String country = channel.get(COUNTRY);
                satChannel.setCategory(channel.get(CATEGORY));
                satChannel.setCountry(country == null || country.length() == 0 ? null : country);
                satChannel.setEncryption(channel.get(ENCRYPTION));
                satChannel.setPolarisation(channel.get(POLARIZATION).charAt(0));
                satChannel.setErrorCorrection(Integer.parseInt(channel.get(FEC)));
                satChannel.setSid(Integer.parseInt(channel.get(SID)));
                satChannel.setSymbolRate(Integer.parseInt(channel.get(SYMBOL_RATE)));

                newMedias.add(satChannel);
            }
        }

    }

}
