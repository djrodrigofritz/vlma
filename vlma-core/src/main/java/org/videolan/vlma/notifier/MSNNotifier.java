/*
 * Copyright (C) 2008-2009 the VideoLAN team
 *
 * This file is part of VLMa.
 *
 * VLMa is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * VLMa is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with VLMa. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package org.videolan.vlma.notifier;

import java.io.IOException;
import java.util.List;

import org.apache.commons.configuration.Configuration;
import org.apache.commons.configuration.event.ConfigurationEvent;
import org.apache.commons.configuration.event.ConfigurationListener;
import org.apache.log4j.Logger;

import rath.msnm.MSNMessenger;
import rath.msnm.SwitchboardSession;
import rath.msnm.UserStatus;
import rath.msnm.msg.MimeMessage;

/**
 * A MSN notifier.
 *
 * @author Adrien Grand <jpountz at videolan.org>
 */
public class MSNNotifier extends Notifier implements ConfigurationListener {

    private static final Logger logger = Logger.getLogger(IRCNotifier.class);

    private static final String LOGIN = "vlma.notification.msn.login";
    private static final String PASS = "vlma.notification.msn.pass";
    private static final String RECIPIENTS = "vlma.notification.msn.recipients";

    private Configuration configuration;
    private MSNMessenger msnm;
    private String login;
    private String pass;
    private List<String> recipients;
    private boolean configurationChanged = false;;

    /* (non-Javadoc)
     * @see org.apache.commons.configuration.event.ConfigurationListener#configurationChanged(org.apache.commons.configuration.event.ConfigurationEvent)
     */
    public void configurationChanged(ConfigurationEvent event) {
        if (!event.isBeforeUpdate()) {
            String key = event.getPropertyName();
            Object value = event.getPropertyValue();
            synchronized (this) {
                if (login != null && LOGIN.equals(key) && !login.equals(value) ||
                        pass != null && PASS.equals(key) && !pass.equals(value) ||
                        recipients != null && RECIPIENTS.equals(key) && !recipients.equals(value)) {
                    logger.info("Configuration of the IRC notifier changed, will reconnect");
                    configurationChanged = true;
                }
            }
        }
    }

    /**
     * Log into MSN.
     */
    @SuppressWarnings("unchecked")
    private void connect() {
        login = configuration.getString(LOGIN);
        pass = configuration.getString(PASS);
        recipients = configuration.getList(RECIPIENTS);
        msnm = new MSNMessenger(login, pass);
        msnm.setInitialStatus(UserStatus.ONLINE);
        msnm.login();
    }

    @Override
    public void init() {
        if(isMSNEnabled()) {
            connect();
        }
        register(this);
    }

    /**
     * Does the user want to be notified by MSN?
     *
     * @return true if there is one recipient or more
     */
    @SuppressWarnings("unchecked")
    private boolean isMSNEnabled() {
        List<String> tmp = configuration.getList("vlma.notification.msn.recipients");
        return (tmp != null && tmp.size() > 0);
    }

    @Override
    public void sendNotification(String message) {
        if (isMSNEnabled()) {
            synchronized (this) {
                if (msnm != null && configurationChanged) {
                    msnm.logout();
                }
                if (msnm == null || !msnm.isLoggedIn()) {
                    connect();
                }
                MimeMessage msg = new MimeMessage();
                msg.setKind(MimeMessage.KIND_MESSAGE);
                msg.setMessage(message);
                SwitchboardSession ss = null;
                for (String recipient : recipients) {
                    ss = null;
                    try {
                        while (ss == null) {
                           ss = msnm.doCallWait(recipient);
                        }
                        ss.sendInstantMessage(msg);
                    } catch (IOException e) {
                        logger.error("Could not send message", e);
                    } catch (InterruptedException e) {
                        logger.error("Could not send message", e);
                    }
                }
            }
        }
    }

    @Override
    public void destroy() {
        unregister(this);
        if (msnm != null && msnm.isLoggedIn())
            msnm.logout();
    }

    /**
     * Sets the configuration.
     *
     * @param configuration the configuration to set
     */
    public void setConfiguration(Configuration configuration) {
        this.configuration = configuration;
    }

}
