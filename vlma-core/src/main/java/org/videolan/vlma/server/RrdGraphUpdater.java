/*
 * Copyright (C) 2006-2009 the VideoLAN team
 *
 * This file is part of VLMa.
 *
 * VLMa is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * VLMa is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with VLMa. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package org.videolan.vlma.server;

import java.awt.Color;
import java.io.File;
import java.io.IOException;
import java.util.Map;

import org.apache.commons.configuration.Configuration;
import org.apache.log4j.Logger;
import org.jrobin.core.RrdDb;
import org.jrobin.core.RrdDbPool;
import org.jrobin.core.RrdDef;
import org.jrobin.core.RrdException;
import org.jrobin.core.Sample;
import org.jrobin.core.Util;
import org.jrobin.graph.RrdGraph;
import org.jrobin.graph.RrdGraphDef;
import org.videolan.vlma.model.Server;
import org.videolan.vlma.monitor.Monitor;
import org.videolan.vlma.server.rpc.DataRetriever;
import org.videolan.vlma.server.rpc.ServerState;

public class RrdGraphUpdater {

    private static final Logger logger = Logger.getLogger(RrdGraphUpdater.class);

    private Configuration configuration;

    private DataRetriever dataRetriever;

    /**
     * Gets the directory where RRD data is saved.
     *
     * @return the RRD folder
     */
    private File getRrdDir() throws IOException {
        String basePath = configuration.getString("vlma.data");
        File rrdDir = new File(basePath, "rrd");
        if (!rrdDir.exists())
            rrdDir.mkdir();
        return rrdDir;
    }

    /**
     * Creates the RRD file if needed and returns its path.
     *
     * @return the RRD file path
     */
    private String createRrdFileIfNecessary(Server server) throws IOException {
        File rrdR = new File(getRrdDir(), server.getName() + ".rrd");
        if (!rrdR.exists()) {
            logger.info("Creating RRD file of " + server.getName());
            try {
                RrdDef rrdDef = new RrdDef(rrdR.getPath());
                rrdDef.setStartTime(Util.getTime());
                rrdDef.addDatasource(ServerState.CPU_LOAD.name().toLowerCase(),    "GAUGE",   300, 0, Double.NaN);
                rrdDef.addDatasource(ServerState.TRAFFIC_IN.name().toLowerCase(),  "COUNTER", 300, 0, Double.NaN);
                rrdDef.addDatasource(ServerState.TRAFFIC_OUT.name().toLowerCase(), "COUNTER", 300, 0, Double.NaN);
                rrdDef.addDatasource(ServerState.VLC_CPU.name().toLowerCase(),     "GAUGE",   300, 0, Double.NaN);
                rrdDef.addDatasource(ServerState.VLC_MEM.name().toLowerCase(),     "GAUGE",   300, 0, Double.NaN);
                rrdDef.addArchive("AVERAGE", 0.5,   1, 4000);
                rrdDef.addArchive("AVERAGE", 0.5,   6, 4000);
                rrdDef.addArchive("AVERAGE", 0.5,  24, 4000);
                rrdDef.addArchive("AVERAGE", 0.5, 288, 4000);
                RrdDbPool rrdPool = RrdDbPool.getInstance();
                RrdDb rrdDb = rrdPool.requestRrdDb(rrdDef);
                rrdPool.release(rrdDb);
            } catch (RrdException e) {
                logger.error("Error while creating RRD file of " + rrdR.getPath(), e);
            } catch (IOException e) {
                logger.error("Error while creating RRD file of " + rrdR.getPath(), e);
            }
            // It seems JRobin does not like several modifications of a RRD
            // file within less than one second
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) { }
        }
        return rrdR.getPath();
    }

    public void updateSnmpData(Server server) {
        String rrdFile;
        try {
            rrdFile = createRrdFileIfNecessary(server);
        } catch (IOException e) {
            logger.error("Error while creating RRD file of " + server.getName(), e);
            return;
        }

        RrdDbPool rrdPool = RrdDbPool.getInstance();
        RrdDb rrdDb = null;
        Sample sample = null;
        try {
            rrdDb = rrdPool.requestRrdDb(rrdFile);
            sample = rrdDb.createSample();
        } catch (Exception e) {
            logger.error("Cannot instanciate the RRD database for server" + server.getName(), e);
        }

        Map<ServerState, Double> serverState = dataRetriever.getServerState(server);
        for(Map.Entry<ServerState, Double> entry : serverState.entrySet()) {
            ServerState data = entry.getKey();
            Double value = entry.getValue();

            try {
                double maxCpuLoad = configuration.getDouble("vlma.notification.cpu_load.threshold");
                double maxVlcCpu = configuration.getDouble("vlma.notification.vlc_cpu.threshold");
                double maxVlcMem = configuration.getDouble("vlma.notification.vlc_mem.threshold");
                double doubleValue = (Double) value;
                switch(data) {
                case CPU_LOAD:
                    if(doubleValue >= maxCpuLoad)
                        Monitor.dispatch(Monitor.ServerEvent.LOAD_TOO_HIGH, server, doubleValue);
                    break;
                case VLC_CPU:
                    if(doubleValue >= maxVlcCpu)
                        Monitor.dispatch(Monitor.ServerEvent.CPU_TOO_HIGH, server, doubleValue);
                    break;
                case VLC_MEM:
                    if(doubleValue >= maxVlcMem)
                        Monitor.dispatch(Monitor.ServerEvent.MEM_TO_HIGH, server, doubleValue);
                    break;
                }
                sample.setValue(data.name().toLowerCase(), doubleValue);
            } catch (Exception e) {
                logger.error("Error while adding value to the RRD database", e);
                // Even if there was an error, update the sample so that every data in the sample has the same timestamp
                try {
                    sample.setValue(data.name().toLowerCase(), 0D);
                } catch (RrdException f) {
                    // Should not happen
                    logger.error("Unexpected error!", f);
                }
            }
        }

        try {
            sample.update();
        } catch (Exception e) {
            logger.error("Cannot update sample of server " + server.getName(), e);
        }
        try {
            rrdPool.release(rrdDb);
        } catch (Exception e) {
            logger.error("Cannot release RRD database for server " + server.getName(), e);
        }
    }

    public void updateRrdGraph(Server server) {
        RrdGraph rrdGraph;
        long startTime, endTime = Util.getTime();
        startTime = endTime - 3600 * 24;

        try {
            File rrdDir = getRrdDir();

            RrdGraphDef graphDef = new RrdGraphDef();
            graphDef.datasource("cpu_load", createRrdFileIfNecessary(server), "cpu_load", "AVERAGE");
            graphDef.area("cpu_load", Color.RED, "CPU load@L");
            graphDef.line("cpu_load", Color.BLUE, "CPU load@L", 3);
            graphDef.gprint("cpu_load", "AVERAGE", "Average CPU load: @3@r");
            graphDef.setLowerLimit(0);
            graphDef.setTimePeriod(startTime, endTime);
            graphDef.setTitle("CPU load");
            rrdGraph = new RrdGraph(graphDef);
            File rrdGraphFile = new File(rrdDir, server.getName() + "-cpu_load.png");
            rrdGraph.saveAsPNG(rrdGraphFile.getAbsolutePath());

            graphDef = new RrdGraphDef();
            graphDef.datasource("traffic_in", createRrdFileIfNecessary(server), "traffic_in", "AVERAGE");
            graphDef.datasource("traffic_out", createRrdFileIfNecessary(server), "traffic_out", "AVERAGE");
            graphDef.line("traffic_in", Color.BLUE, "Incoming traffic", 3);
            graphDef.line("traffic_out", Color.RED, "Outgoing traffic", 3);
            graphDef.setLowerLimit(0);
            graphDef.setTimePeriod(startTime, endTime);
            graphDef.setTitle("Traffic");
            rrdGraph = new RrdGraph(graphDef);
            rrdGraphFile = new File(rrdDir, server.getName() + "-traffic.png");
            rrdGraph.saveAsPNG(rrdGraphFile.getAbsolutePath());

            graphDef = new RrdGraphDef();
            graphDef.datasource("vlc_cpu", createRrdFileIfNecessary(server), "vlc_cpu", "AVERAGE");
            graphDef.datasource("vlc_mem", createRrdFileIfNecessary(server), "vlc_mem", "AVERAGE");
            graphDef.line("vlc_cpu", Color.RED, "CPU usage for VLC", 3);
            graphDef.line("vlc_mem", Color.BLUE, "Memory usage for VLC", 3);
            graphDef.setLowerLimit(0);
            graphDef.setTimePeriod(startTime, endTime);
            graphDef.setTitle("Resources used for VLC");
            rrdGraph = new RrdGraph(graphDef);
            rrdGraphFile = new File(rrdDir, server.getName() + "-vlc.png");
            rrdGraph.saveAsPNG(rrdGraphFile.getAbsolutePath());
        } catch (RrdException e) {
            logger.error("Error while creating RRD file of " + server.getName(), e);
        } catch (IOException e) {
            logger.error("Error while creating RRD file of " + server.getName(), e);
        }
    }

    /**
     * @param configuration the configuration to set
     */
    public void setConfiguration(Configuration configuration) {
        this.configuration = configuration;
    }

    /**
     * @param dataRetriever the dataRetriever to set
     */
    public void setDataRetriever(DataRetriever dataRetriever) {
        this.dataRetriever = dataRetriever;
    }

}
