/*
 * Copyright (C) 2008 the VideoLAN team
 *
 * This file is part of VLMa.
 *
 * VLMa is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * VLMa is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with VLMa. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package org.videolan.vlma.server.rpc;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import org.apache.commons.configuration.Configuration;
import org.videolan.vlma.exception.NotFoundException;
import org.videolan.vlma.model.Server;

public class DataRetrieverDispatcher implements DataRetriever {

    private Configuration configuration;
    private DataRetriever dummyDataRetriever;
    private DataRetriever snmpDataRetriever;
    private DataRetriever httpDataRetriever;

    private DataRetriever getDataRetriever() {
        String stringMethod = configuration.getString("vlc.monitor.impl");
        Method method = Method.valueOf(stringMethod);
        switch(method) {
        case HTTP:
            return httpDataRetriever;
        case SNMP:
            return snmpDataRetriever;
        case NONE:
            return dummyDataRetriever;
        default:
            throw new NotFoundException("No dataRetriever found for method " + stringMethod);
        }
    }

    public Map<ServerState, Double> getServerState(Server server) {
        return getDataRetriever().getServerState(server);
    }

    public List<String> getVlcLogTail(Server server) throws IOException {
        return getDataRetriever().getVlcLogTail(server);
    }

    public String getVlcVersion(Server server) throws IOException {
        return getDataRetriever().getVlcVersion(server);
    }

    public boolean restartVlc(Server server) throws IOException {
        return getDataRetriever().restartVlc(server);
    }

    public Long getVlcUptime(Server server) throws IOException {
        return getDataRetriever().getVlcUptime(server);
    }

    /**
     * @param configuration the configuration to set
     */
    public void setConfiguration(Configuration configuration) {
        this.configuration = configuration;
    }

    /**
     * @param dummyDataRetriever the dummyDataRetriever to set
     */
    public void setDummyDataRetriever(DataRetrieverMockImpl dummyDataRetriever) {
        this.dummyDataRetriever = dummyDataRetriever;
    }

    /**
     * @param httpDataRetriever the httpDataRetriever to set
     */
    public void setHttpDataRetriever(HttpDataRetriever httpDataRetriever) {
        this.httpDataRetriever = httpDataRetriever;
    }

    /**
     * @param snmpDataRetriever the snmpDataRetriever to set
     */
    public void setSnmpDataRetriever(SnmpDataRetriever snmpDataRetriever) {
        this.snmpDataRetriever = snmpDataRetriever;
    }

}
